-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 16, 2018 at 03:39 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.1.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sumber_air`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_ekonomi`
--

CREATE TABLE `t_ekonomi` (
  `id` int(11) NOT NULL,
  `id_sa` varchar(50) NOT NULL,
  `keberadaan_iuran` enum('ada','tidak ada') NOT NULL,
  `besaran_tarif` varchar(30) NOT NULL,
  `minat_utk_peningkat` enum('minat','tidak minat') NOT NULL,
  `peningkatan_retribusi` enum('berminat','tidak bersedia') NOT NULL,
  `besaran_retribusi` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_ekonomi`
--

INSERT INTO `t_ekonomi` (`id`, `id_sa`, `keberadaan_iuran`, `besaran_tarif`, `minat_utk_peningkat`, `peningkatan_retribusi`, `besaran_retribusi`) VALUES
(1, 'id_sa', '', 'besaran_tarif', '', '', 'besaran_retribusi'),
(2, 'SA2', 'ada', '3000', 'minat', '', '5000'),
(3, 'SA3', 'tidak ada', '', '', '', ''),
(4, 'SA4', 'ada', '5000', 'minat', '', '6000'),
(5, 'SA5', 'ada', '2000/kubik', 'minat', '', ''),
(6, 'SA6', 'ada', '10.000/ 10 hari', '', '', ''),
(7, 'SA7', 'tidak ada', '', '', '', ''),
(8, 'SA8', 'tidak ada', '', '', '', ''),
(9, 'SA9', '', '', '', '', ''),
(10, 'SA10', 'tidak ada', '', 'minat', '', ''),
(11, 'SA11', 'tidak ada', '', '', '', ''),
(12, 'SA12', '', '', '', '', ''),
(13, 'SA13', 'tidak ada', '', '', '', ''),
(14, 'SA14', 'tidak ada', '', '', '', ''),
(15, 'SA15', 'tidak ada', '', '', '', ''),
(16, 'SA16', '', '', '', '', ''),
(17, 'SA17', '', '', '', '', ''),
(18, 'SA18', '', '', '', '', ''),
(19, 'SA19', '', '', '', '', ''),
(20, 'SA20', '', '', '', '', ''),
(21, 'SA21', '', '', '', '', ''),
(22, 'SA22', '', '', '', '', ''),
(23, 'SA23', 'tidak ada', '', '', '', ''),
(24, 'SA24', 'tidak ada', '', '', '', ''),
(25, 'SA25', 'tidak ada', '', '', '', ''),
(26, 'SA26', 'tidak ada', '', '', '', ''),
(27, 'SA27', 'tidak ada', '', '', '', ''),
(28, 'SA28', 'ada', '2000-5000 per bulan', 'minat', '', ''),
(29, 'SA29', 'tidak ada', 'Rp. 20.000,00', '', 'berminat', ''),
(30, 'SA30', 'ada', '2.000 - 10.000', '', '', ''),
(31, 'SA31', 'tidak ada', '', '', 'tidak bersedia', ''),
(32, 'SA32', 'tidak ada', '', '', 'tidak bersedia', ''),
(34, 'SA34', 'tidak ada', '', '', 'tidak bersedia', ''),
(35, 'SA35', 'tidak ada', '', '', 'tidak bersedia', ''),
(36, 'SA36', 'tidak ada', '', '', 'tidak bersedia', ''),
(37, 'SA37', 'tidak ada', '', '', 'tidak bersedia', ''),
(38, 'SA38', 'tidak ada', '', '', 'tidak bersedia', ''),
(39, 'SA39', 'tidak ada', '', '', 'tidak bersedia', ''),
(40, 'SA40', 'tidak ada', '', '', 'tidak bersedia', ''),
(41, 'SA41', 'ada', '', '', '', ''),
(42, 'SA42', 'tidak ada', '', '', 'tidak bersedia', ''),
(43, 'SA43', 'tidak ada', '', '', 'tidak bersedia', ''),
(44, 'SA44', 'tidak ada', '', '', 'tidak bersedia', ''),
(45, 'SA45', 'tidak ada', '', '', 'tidak bersedia', ''),
(46, 'SA46', 'tidak ada', '', '', 'tidak bersedia', ''),
(47, 'SA47', 'tidak ada', '', '', 'tidak bersedia', ''),
(48, 'SA48', '', '-', '', '', ''),
(49, 'SA49', '', '-', '', '', ''),
(50, 'SA50', '', '-', '', '', ''),
(51, '', 'tidak ada', '', '', 'tidak bersedia', ''),
(52, '', 'tidak ada', '', '', 'tidak bersedia', ''),
(53, '', 'tidak ada', '', '', 'tidak bersedia', ''),
(54, '', 'tidak ada', '', '', 'tidak bersedia', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_gambaran_daerah_pelayanan`
--

CREATE TABLE `t_gambaran_daerah_pelayanan` (
  `id` int(50) NOT NULL,
  `id_sa` varchar(50) NOT NULL,
  `gbr_umum` text NOT NULL,
  `hambatan_teknis` text NOT NULL,
  `hambatan_non_teknis` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_gambaran_daerah_pelayanan`
--

INSERT INTO `t_gambaran_daerah_pelayanan` (`id`, `id_sa`, `gbr_umum`, `hambatan_teknis`, `hambatan_non_teknis`) VALUES
(1, 'id_sa', 'gbr_umum', 'hambatan_teknis', 'hambatan_non_teknis'),
(2, 'SA2', '-', 'Pembuangan sampah (Tissue, botol,dll) sehingga menyumbat saluran air.', '-'),
(3, 'SA3', '-', 'instalasi dan perawatan pipa tidak berjalan', 'sumber mata air ini akan dijadikan kawasan niaga keluarahan sempur'),
(4, 'SA4', '-', 'meteran belum ada dan jaringan tersumbat', 'penggunaan token listrik tidak mencukupi dari retribusi'),
(5, 'SA5', 'hidran berada di tepi sungai, dan air bersih ini melayani permukiman padat penduduk.', 'kualitas teknis tidak memadai', 'pembayaran dari warga terkadang tidak sesuai aturan'),
(6, 'SA6', 'hidran berada di belakang mesjid, kondisi rusak, dan tidak berfungsi sama sekali', 'air tidak jalan', 'air berbau'),
(7, 'SA7', 'kawasan padat penduduk', '', 'air tidak ada'),
(8, 'SA8', '-', '', '-'),
(9, 'SA9', '-', '', '-'),
(10, 'SA10', '-', 'terjadi longsoran sehingga batuan memecah pipa air', 'kena longsor'),
(11, 'SA11', '-', '', 'longsor'),
(12, 'SA12', '-', '', ''),
(13, 'SA13', '-', 'tidak ada pengolahan air', 'longsor '),
(14, 'SA14', '-', '', ''),
(15, 'SA15', '-', '', 'longsor, dan luapan air sungai'),
(16, 'SA16', '-', '', 'lereng curam'),
(17, 'SA17', '-', 'belum ada bak penampungan', 'longsor '),
(18, 'SA18', '-', 'biaya listrik besar', '-'),
(19, 'SA19', '-', 'harus dikeruk', '-'),
(20, 'SA20', '-', '-', '-'),
(21, 'SA21', '-', '-', '-'),
(22, 'SA22', '-', '-', 'longsor, sampah disekitar lingkungan'),
(23, 'SA23', 'mata air berada di daerah aliran sungai', '-', 'warga tidak bisa merawat dengan baik'),
(24, 'SA24', '-', '-', 'warga tidak bisa merawat dengan baik'),
(25, 'SA25', '-', '-', 'warga tidak bisa merawat dengan baik'),
(26, 'SA26', '-', '-', 'warga tidak bisa merawat dengan baik'),
(27, 'SA27', '-', '-', 'warga tidak bisa merawat dengan baik'),
(28, 'SA28', '-', 'mesin rusak', '-'),
(29, 'SA29', '-', '-', '-'),
(30, 'SA30', '-', '-', '-'),
(31, 'SA31', '-', '-', '-'),
(32, 'SA32', 'Masyarakat Sekita menggunakan Sumur gali dan PDAM', 'Pipa mati sehingga tidak bisa mengalirkan air', '-'),
(34, 'SA34', 'Masyarakat Sekitar menggunakan PIPA PDAM', '', ''),
(35, 'SA35', 'Masyarakat Sekitar menggunakan PIPA PDAM dan Sumur', '', ''),
(36, 'SA36', 'Masyarakat Sekitar menggunakan PIPA PDAM dan Sumur', '', ''),
(37, 'SA37', 'Masyarakat Sekitar menggunakan PIPA PDAM dan Sumur', '', ''),
(38, 'SA38', 'Masyarakat Sekitar menggunakan PIPA PDAM dan Sumur', '', ''),
(39, 'SA39', 'Masyarakat Sekitar menggunakan PIPA PDAM dan Sumur', '', ''),
(40, 'SA40', 'Masyarakat Sekitar menggunakan PIPA PDAM dan Sumur', '', 'Iuran Biaya perawatan tidak tersedia'),
(41, 'SA41', 'Masyarakat Sekitar menggunakan PIPA PDAM dan Sumur', 'Kualitas Air tidak terlalu baik', ''),
(42, 'SA42', 'Masyarakat Sekitar menggunakan PIPA PDAM dan Sumur', 'Kualitas Air tidak terlalu baik', ''),
(43, 'SA43', 'Masyarakat Sekitar menggunakan  Sumur', '', ''),
(44, 'SA44', 'Masyarakat Sekitar menggunakan  PDAM', '', ''),
(45, 'SA45', 'Masyarakat Sekitar menggunakan  Sumur dan PDAM', '', ''),
(46, 'SA46', 'Masyarakat Sekitar menggunakan  Sumur', '', ''),
(47, 'SA47', 'Masyarakat Sekitar menggunakan  Sumur', '', ''),
(48, 'SA48', 'Masyarakat Sekitar menggunakan  Sumur', '', ''),
(49, 'SA49', 'Masyarakat Sekitar menggunakan  Sumur', '', ''),
(50, 'SA50', 'Masyarakat Sekitar menggunakan  Sumur', '', ''),
(51, 'SA32', 'Masyarakat Sekita menggunakan Sumur gali dan PDAM', 'Pipa mati sehingga tidak bisa mengalirkan air', '-'),
(52, 'SA32', 'Masyarakat Sekita menggunakan Sumur gali dan PDAM', 'Pipa mati sehingga tidak bisa mengalirkan air', '-'),
(53, 'SA32', 'Masyarakat Sekita menggunakan Sumur gali dan PDAM', 'Pipa mati sehingga tidak bisa mengalirkan air', '-'),
(54, 'SA32', 'Masyarakat Sekita menggunakan Sumur gali dan PDAM', 'Pipa mati sehingga tidak bisa mengalirkan air', '-');

-- --------------------------------------------------------

--
-- Table structure for table `t_jaringan_pipa`
--

CREATE TABLE `t_jaringan_pipa` (
  `id` int(50) NOT NULL,
  `id_sa` varchar(50) NOT NULL,
  `jml_sr_terpasang` varchar(30) NOT NULL,
  `kondisi_kelancaran_sis_pengalir` enum('lancar','tidak lancar') NOT NULL,
  `kondisi_perawatan_sis_pengalir` enum('terawat','tidak terawat') NOT NULL,
  `kondisi_fungsi_sis_pengalir` enum('optimal','belum optimal') NOT NULL,
  `ketersediaan_hidran` enum('ada','tidak ada') NOT NULL,
  `jml_hidran` varchar(15) NOT NULL,
  `meteran` enum('ada','tidak ada') NOT NULL,
  `jml_meteran` varchar(15) NOT NULL,
  `pemeliharaan_sis_jaringan` enum('ada dan rutin','ada dan tidak rutin','tidak ada') NOT NULL,
  `penjelasan_jar_pipa` text NOT NULL,
  `lebar_jln` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jaringan_pipa`
--

INSERT INTO `t_jaringan_pipa` (`id`, `id_sa`, `jml_sr_terpasang`, `kondisi_kelancaran_sis_pengalir`, `kondisi_perawatan_sis_pengalir`, `kondisi_fungsi_sis_pengalir`, `ketersediaan_hidran`, `jml_hidran`, `meteran`, `jml_meteran`, `pemeliharaan_sis_jaringan`, `penjelasan_jar_pipa`, `lebar_jln`) VALUES
(1, 'id_sa', 'jml_sr_terpasang', '', '', '', '', 'jml_hidran', '', 'jml_meteran', '', 'penjelasan', 'lebar_jln'),
(2, 'SA2', '56', 'lancar', 'terawat', 'optimal', 'ada', '1', 'tidak ada', '', '', 'ada kelompok yang mengelola', '1.3'),
(3, 'SA3', '', '', 'tidak terawat', 'belum optimal', 'tidak ada', '0', 'tidak ada', '', 'tidak ada', 'sifat pemakaian bersama dan pengelolaan dari kelurahan tidak ada.', '1.1'),
(4, 'SA4', '32', 'lancar', 'tidak terawat', 'belum optimal', 'tidak ada', '0', 'tidak ada', '', 'tidak ada', 'Distribusi/instalasi air hanya menyalurkan saja, sistem pengolahan dari warga berupa saringan saja', '1.35'),
(5, 'SA5', '50', 'lancar', 'terawat', 'optimal', 'ada', '1', 'ada', '50 unit', '', 'pemeliharaan setiap sebulan sekali', '1.2'),
(6, 'SA6', '36', '', 'tidak terawat', 'belum optimal', 'ada', '1', 'tidak ada', '', 'tidak ada', '', '1.2'),
(7, 'SA7', '50', '', 'tidak terawat', 'belum optimal', 'ada', '1', 'tidak ada', '', 'tidak ada', 'Dulu ada meteran di setiap rumah, sekarang sudah tidak ada karena SAB tidak berfungsi', '1.4'),
(8, 'SA8', '', 'lancar', 'terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', '', '0.8'),
(9, 'SA9', '', '', '', '', '', '', '', '', '', '', ''),
(10, 'SA10', '6', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'hanya dilakukan apabila terjadi kerusakan', '1.4'),
(11, 'SA11', '', '', '', '', '', '', '', '', '', 'mata air hanya dibuat paralon dan tidak ada pengelolaan sama sekali', '0.8'),
(12, 'SA12', '', '', '', '', '', '', '', '', '', 'mata air hanya dibuat paralon dan tidak ada pengelolaan sama sekali', '1'),
(13, 'SA13', '', '', '', '', '', '', '', '', '', 'air mengalir langsung dari mata air, belum ada perpipaan', '2.3'),
(14, 'SA14', '', '', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', 'air tidak ada', '1.3'),
(15, 'SA15', '10', 'lancar', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', '', 'pengelolan apabila terjadi kerusakan ', '1.7'),
(16, 'SA16', '', '', '', '', '', '', '', '', '', 'belum didistribusikan', '2 M'),
(17, 'SA17', '48', 'lancar', '', '', 'tidak ada', '', 'tidak ada', '', '', 'pendistribusian hanya berupa selang dari masyarakat', '1.2'),
(18, 'SA18', '3', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', '', '2'),
(19, 'SA19', '20', 'lancar', '', 'belum optimal', 'tidak ada', '', '', '', '', '', ''),
(20, 'SA20', '', '', '', '', '', '', '', '', '', '', ''),
(21, 'SA21', '', '', '', '', '', '', '', '', '', '', ''),
(22, 'SA22', '', 'lancar', 'terawat', 'belum optimal', 'tidak ada', '', '', '', '', '', ''),
(23, 'SA23', '', '', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', 'air belum didistribusikan kerumah warga', '0.8'),
(24, 'SA24', '', '', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', 'air belum didistribusikan kerumah warga', '3'),
(25, 'SA25', '', 'lancar', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', 'mata air sangat bagus, dan lancar apabila di bangun dan dikelola dengan baik', '1.5'),
(26, 'SA26', '', 'lancar', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', 'mata air sangat bagus, dan lancar apabila di bangun dan dikelola dengan baik', '1.5'),
(27, 'SA27', '', 'lancar', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', 'mata air sangat bagus, dan lancar apabila di bangun dan dikelola dengan baik', '1.5'),
(28, 'SA28', '', 'lancar', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', '', '1'),
(29, 'SA29', '', '', '', '', '', '', '', '', '', '', ''),
(30, 'SA30', '', '', '', '', '', '', '', '', '', '', ''),
(31, 'SA31', '', '', 'tidak terawat', 'belum optimal', 'tidak ada', '', 'tidak ada', '', 'tidak ada', '', '2'),
(32, 'SA32', '13', 'tidak lancar', 'terawat', 'belum optimal', 'tidak ada', '', 'ada', '11', 'tidak ada', 'Pemeliharaan tidak ada karena SPAM tidak berjalan', '1.5'),
(34, 'SA34', '', '', '', '', '', '', '', '', '', '', '1'),
(35, 'SA35', '', '', '', '', '', '', '', '', '', '', '1'),
(36, 'SA46', '', '', '', '', '', '', '', '', '', '', '1'),
(37, 'SA37', '', '', '', '', '', '', '', '', '', '', '1'),
(38, 'SA38', '', '', '', '', '', '', '', '', '', '', '1'),
(39, 'SA39', '', '', '', '', '', '', '', '', '', '', '1'),
(40, 'SA40', '20', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Dilakukan masing-masing', '1'),
(41, 'SA41', '15', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Pembersihan di sesuaikan dengan kondisi bak penampung', '1'),
(42, 'SA42', '14', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan 1 minggu sekali', '1'),
(43, 'SA43', '15', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan Rutin', '2'),
(44, 'SA44', '', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan Rutin sudah ada pengelola', '2'),
(45, 'SA45', '15', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan Rutin', '2'),
(46, 'SA46', '15', 'lancar', 'terawat', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan di seseuaikan dengan kondisi SPAM', '1'),
(47, 'SA47', '', '', '', '', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan di seseuaikan dengan kondisi SPAM', ''),
(48, 'SA48', '', 'lancar', '', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan di seseuaikan dengan kondisi SPAM', '1'),
(49, 'SA49', '', 'lancar', '', 'optimal', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan di seseuaikan dengan kondisi SPAM', '1'),
(50, 'SA50', '-', '', '', '', 'tidak ada', '', 'tidak ada', '', '', 'Pemeliharaan di seseuaikan dengan kondisi SPAM', '1'),
(51, '', '13', 'tidak lancar', 'terawat', 'belum optimal', 'tidak ada', '', 'ada', '11', 'tidak ada', '', ''),
(52, '', '13', 'tidak lancar', 'terawat', 'belum optimal', 'tidak ada', '', 'ada', '11', 'tidak ada', '', ''),
(53, '', '13', 'tidak lancar', 'terawat', 'belum optimal', 'tidak ada', '', 'ada', '11', 'tidak ada', '', ''),
(54, '', '13', 'tidak lancar', 'terawat', 'belum optimal', 'tidak ada', '', 'ada', '11', 'tidak ada', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_kecamatan`
--

CREATE TABLE `t_kecamatan` (
  `id_kecamatan` varchar(7) NOT NULL,
  `kabupaten_id` varchar(4) NOT NULL,
  `nm_kecamatan` varchar(30) NOT NULL,
  `lat` varchar(50) NOT NULL,
  `lon` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_kecamatan`
--

INSERT INTO `t_kecamatan` (`id_kecamatan`, `kabupaten_id`, `nm_kecamatan`, `lat`, `lon`) VALUES
('3271010', '3271', ' Bogor Selatan', '', ''),
('3271020', '3271', ' Bogor Timur', '', ''),
('3271030', '3271', ' Bogor Utara', '', ''),
('3271040', '3271', ' Bogor Tengah', '', ''),
('3271050', '3271', ' Bogor Barat', '', ''),
('3271060', '3271', ' Tanah Sereal', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_kelurahan`
--

CREATE TABLE `t_kelurahan` (
  `id_desa` varchar(10) NOT NULL,
  `kecamatan_id` varchar(7) NOT NULL,
  `nm_desa` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_kelurahan`
--

INSERT INTO `t_kelurahan` (`id_desa`, `kecamatan_id`, `nm_desa`) VALUES
('3271010001', '3271010', 'Mulyaharja'),
('3271010002', '3271010', 'Pamoyanan'),
('3271010003', '3271010', 'Ranggamekar'),
('3271010004', '3271010', 'Genteng'),
('3271010005', '3271010', 'Kertamaya'),
('3271010006', '3271010', 'Rancamaya'),
('3271010007', '3271010', 'Bojongkerta'),
('3271010008', '3271010', 'Harjasari'),
('3271010009', '3271010', 'Muarasari'),
('3271010010', '3271010', 'Pakuan'),
('3271010011', '3271010', 'Cipaku'),
('3271010012', '3271010', 'Lawanggintung'),
('3271010013', '3271010', 'Batutulis'),
('3271010014', '3271010', 'Bondongan'),
('3271010015', '3271010', 'Empang'),
('3271010016', '3271010', 'Cikaret'),
('3271020001', '3271020', 'Sindangsari'),
('3271020002', '3271020', 'Sindangrasa'),
('3271020003', '3271020', 'Tajur'),
('3271020004', '3271020', 'Katulampa'),
('3271020005', '3271020', 'Baranangsiang'),
('3271020006', '3271020', 'Sukasari'),
('3271030001', '3271030', 'Bantarjati'),
('3271030002', '3271030', 'Tegalgundil'),
('3271030003', '3271030', 'Tanahbaru'),
('3271030004', '3271030', 'Cimahpar'),
('3271030005', '3271030', 'Ciluar'),
('3271030006', '3271030', 'Cibuluh'),
('3271030007', '3271030', 'Kedunghalang'),
('3271030008', '3271030', 'Ciparigi'),
('3271040001', '3271040', 'Paledang'),
('3271040002', '3271040', 'Gudang'),
('3271040003', '3271040', 'Babakanpasar'),
('3271040004', '3271040', 'Tegallega'),
('3271040005', '3271040', 'Babakan'),
('3271040006', '3271040', 'Sempur'),
('3271040007', '3271040', 'Pabaton'),
('3271040008', '3271040', 'Cibogor'),
('3271040009', '3271040', 'Panaragan'),
('3271040010', '3271040', 'Kebonkelapa'),
('3271040011', '3271040', 'Ciwaringin'),
('3271050001', '3271050', 'Pasirmulya'),
('3271050002', '3271050', 'Pasirkuda'),
('3271050003', '3271050', 'Pasirjaya'),
('3271050004', '3271050', 'Gunungbatu'),
('3271050005', '3271050', 'Loji'),
('3271050006', '3271050', 'Menteng'),
('3271050007', '3271050', 'Cilendek Timur'),
('3271050008', '3271050', 'Cilendek Barat'),
('3271050009', '3271050', 'Sindangbarang'),
('3271050010', '3271050', 'Margajaya'),
('3271050011', '3271050', 'Balungbangjaya'),
('3271050012', '3271050', 'Situgede'),
('3271050013', '3271050', 'Bubulak'),
('3271050014', '3271050', 'Semplak'),
('3271050015', '3271050', 'Curugmekar'),
('3271050016', '3271050', 'Curug'),
('3271060001', '3271060', 'Kedungwaringin'),
('3271060002', '3271060', 'Kedungjaya'),
('3271060003', '3271060', 'Kebonpedes'),
('3271060004', '3271060', 'Tanahsareal'),
('3271060005', '3271060', 'Kedungbadak'),
('3271060006', '3271060', 'Sukaresmi'),
('3271060007', '3271060', 'Sukadamai'),
('3271060008', '3271060', 'Cibadak'),
('3271060009', '3271060', 'Kayumanis'),
('3271060010', '3271060', 'Mekarwangi'),
('3271060011', '3271060', 'Kencana');

-- --------------------------------------------------------

--
-- Table structure for table `t_lokasi`
--

CREATE TABLE `t_lokasi` (
  `id` int(20) NOT NULL,
  `id_sa` varchar(50) NOT NULL,
  `kecamatan` varchar(7) NOT NULL,
  `kelurahan` varchar(10) NOT NULL,
  `rt` varchar(5) NOT NULL,
  `rw` varchar(5) NOT NULL,
  `almt` text NOT NULL,
  `no_sk_lh` varchar(50) NOT NULL,
  `no_sk_kel` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `latitude` varchar(50) NOT NULL,
  `ls_lahan` varchar(15) NOT NULL,
  `ls_lahan_terbangun` varchar(15) NOT NULL,
  `perkiraan_sisa_lhn` varchar(15) NOT NULL,
  `status_lahan` varchar(30) NOT NULL,
  `pendanaan` varchar(30) NOT NULL,
  `thn_pembangunan` varchar(10) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_lokasi`
--

INSERT INTO `t_lokasi` (`id`, `id_sa`, `kecamatan`, `kelurahan`, `rt`, `rw`, `almt`, `no_sk_lh`, `no_sk_kel`, `longitude`, `latitude`, `ls_lahan`, `ls_lahan_terbangun`, `perkiraan_sisa_lhn`, `status_lahan`, `pendanaan`, `thn_pembangunan`, `foto`) VALUES
(1, 'id_sa', 'kecamat', 'kelurahan', 'rt', 'rw', 'almt', 'no_sk_lh', 'no_sk_kel', 'longitude', 'latitude', 'ls_lahan', 'ls_lahan_terban', 'perkiraan_sisa_', 'status_lahan', 'pendanaan', 'thn_pemban', ''),
(2, 'SA2', '3271040', '3271040006', '1', '4', 'Kampung Rambutan', 'Tidak Ada', 'Tidak Ada', '-6.5872', '106.7975', '36', '36', '-', 'Hibah', 'APBD', '2016', ''),
(3, 'SA3', '3271040', '3271040006', '1', '4', 'Kampung Rambutan', 'Tidak Ada', 'Tidak Ada', '-6.5881', '106.7974', '15', '15', '-', 'Milik Pemerintah', 'APBN', '2013', ''),
(4, 'SA4', '3271040', '3271040006', '1', '4', 'Kampung Cilio', 'Tidak Ada', 'Tidak Ada', '-6.5867', '106.7974', '40', '28', '12', 'Hibah', 'Swadaya Masyarakat', '1970-an', ''),
(5, 'SA5', '3271050', '3271050010', '1', '3', 'Kampung Dramaga', 'Tidak Ada', 'Tidak Ada', '-6.5737', '106.7443', '100', '100', '-', 'Hibah', 'APBD', '2010', ''),
(6, 'SA6', '3271050', '3271050010', '2', '5', 'Kampung Dramaga Kaum', 'Tidak Ada', 'Tidak Ada', '-6.574', '106.7405', '20', '9', '11', 'hibah', 'APBD', '2014', ''),
(7, 'SA7', '3271050', '3271050010', '2', '4', 'Kampung Dramaga Lonceng', 'Tidak Ada', 'Tidak Ada', '-6.5753', '106.744', '15', '12', '3', 'Milik Masyarakat', 'APBD', '2010', ''),
(8, 'SA8', '3271040', '3271040008', '1', '2', 'Kampung Ciwaringin Tanah Sewa', 'Tidak Ada', 'Tidak Ada', '-6.5844', '106.7928', '12', '9', '3', 'DAS', 'Swadaya Masyarakat', '2002', ''),
(9, 'SA9', '3271040', '3271040008', '1', '2', 'Kampung Cibogor', 'Tidak Ada', 'Tidak Ada', '-6.5844', '106.7928', '25', '5', '20', '', '', '', ''),
(10, 'SA10', '3271040', '3271040008', '1', '4', 'Kampung Cibogor', 'Tidak Ada', 'Tidak Ada', '-6.5849', '106.7933', '20', '14.5', '5.5', 'Milik Masyarakat', 'APBD', '2016', ''),
(11, 'SA11', '3271040', '3271040008', '2', '5', 'Kampung Ardio', 'Tidak Ada', 'Tidak Ada', '-6.5862', '106.7907', '30', '24', '6', 'DAS', 'lainnya', '', ''),
(12, 'SA12', '3271040', '3271040008', '3', '5', 'Kampung Ardio', 'Tidak Ada', 'Tidak Ada', '-6.5849', '106.7907', '50', '30', '20', 'Hibah', '', '', ''),
(13, 'SA13', '3271040', '3271040008', '4', '5', 'Kampung Ardio', 'Tidak Ada', 'Tidak Ada', '-6.5873', '106.7908', '15', '15', '-', 'Milik pemerintah', 'APBD', '2010', ''),
(14, 'SA14', '3271040', '3271040008', '3', '3', 'Kampung Ambesin', 'Tidak Ada', 'Tidak Ada', '-6.5847', '106.7934', '6', '6', '-', 'Hibah', 'APBD', '2011', ''),
(15, 'SA15', '3271040', '3271040008', '3', '6', 'Gang Hj Sarmidi', 'Tidak Ada', 'Tidak Ada', '-6.5883', '106.7907', '15', '15', '-', '', '', '', ''),
(16, 'SA16', '3271040', '3271040008', '4', '4', 'Gang Masjid', 'Tidak Ada', 'Tidak Ada', '-6.5855', '106.7937', '20', '15', '5', 'Milik Masyarakat', 'Lainnya', '2009', ''),
(17, 'SA17', '3271050', '3271050006', '4', '15', 'Kampung Pertanian', 'Tidak Ada', 'Tidak Ada', '-6.5756', '106.7837', '24', '20', '4', 'Milik Masyarakat', 'Swadaya Masyarakat', '70 an', ''),
(18, 'SA18', '3271050', '3271050006', '3', '10', 'Gang Kelor', 'Tidak Ada', 'Tidak Ada', '-6.5797', '106.7716', '160', '40', '120', 'hibah', 'APBD', '1990 an', ''),
(19, 'SA19', '3271050', '3271050006', '1', '10', 'Gang Kelor', 'Tidak Ada', 'Tidak Ada', '-6.5806', '106.7726', '30', '24', '6', 'fasum', 'Swadaya Masyarakat', '1980 an', ''),
(20, 'SA20', '3271050', '3271050006', '4', '11', 'Pemutihan', 'Tidak Ada', 'Tidak Ada', '-6.5774', '106.7842', '35', '24', '11', 'fasum', 'PNPM', '', ''),
(21, 'SA21', '3271050', '3271050006', '3', '11', 'Pemutihan', 'Tidak Ada', 'Tidak Ada', '-6.5767', '106.7828', '40', '24', '16', 'fasum', 'Swadaya Masyarakat', '2016', ''),
(22, 'SA22', '3271050', '3271050006', '2', '7', 'Kelapa Sanggeh', 'Tidak Ada', 'Tidak Ada', '-6.5873', '106.787', '24', '24', '-', 'milik pemerintah', 'APBD', '2015', ''),
(23, 'SA23', '3271040', '3271040002', '2', '5', 'Gang Aut', 'Tidak Ada', 'Tidak Ada', '-6.6116', '106.8035', '20', '18', '2', 'DAS', 'APBD', '2008', ''),
(24, 'SA24', '3271040', '3271040002', '2', '5', 'Gang Aut', 'Tidak Ada', 'Tidak Ada', '-6.6116', '106.804', '30', '18', '12', 'DAS', 'APBD', '2008', ''),
(25, 'SA25', '3271040', '3271040002', '3', '3', 'Kampung Padasuka', 'Tidak Ada', 'Tidak Ada', '-6.6088', '106.8009', '25', '12', '13', 'Milik Masyarakat', 'Swadaya Masyarakat', '2008', ''),
(26, 'SA26', '3271040', '3271040002', '3', '3', 'Kampung Padasuka', 'Tidak Ada', 'Tidak Ada', '-6.6088', '106.8009', '14', '10', '4', 'Milik Masyarakat', 'BPLH', '2012', ''),
(27, 'SA27', '3271040', '3271040002', '3', '3', 'Kampung Padasuka', 'Tidak Ada', 'Tidak Ada', '-6.6088', '106.8009', '20', '15', '5', 'Milik Masyarakat', 'Swadaya Masyarakat', '2005', ''),
(28, 'SA28', '3271040', '3271040002', '2', '11', 'Kampung Padasuka', 'Tidak Ada', 'Tidak Ada', '-6.6081', '106.7994', '18', '8', '10', 'DAS', 'APBD', '2011', ''),
(29, 'SA29', '3271040', '3271040002', '3', '11', 'Kebon Pala', 'Tidak Ada', 'Tidak Ada', '-6.6085', '106.7998', '15', '15', '-', 'DAS', 'APBD', '2014', ''),
(30, 'SA30', '3271040', '3271040002', '3', '11', 'Kebon Pala', 'Tidak Ada', 'Tidak Ada', '-6.6086', '106.7996', '16', '16', '-', 'Lahan Masjid', 'Swadaya Masyarakat', '2001', ''),
(31, 'SA31', '3271040', '3271040002', '3', '2', 'Kampung cincau', 'Tidak Ada', 'Tidak Ada', '-6.6075', '106.7991', '20', '16', '4', 'Milik Masyarakat', 'P2KP', '2001', ''),
(34, 'SA34', '3271010', '3271010011', '3', '7', 'Cipaku Haji', '', '', '-6.63335', '106.81793', '30', '8', '22', 'Milik Masyarakat', '', '', ''),
(35, 'SA35', '3271010', '3271010010', '3', '3', 'Babakan Anyar', '', '', '-6.63055', '106.82128', '40', '4.2', '35.8', 'Milik Masyarakat', '', '', ''),
(36, 'SA36', '3271010', '3271010008', '4', '5', 'Jl. Rulita', '', '', '-6.653', '106.84264', '30', '9', '21', 'Milik Masyarakat', '', '', ''),
(37, 'SA37', '3271010', '3271010008', '1', '11', 'Indah Sari', '', '', '-6.64735', '106.83676', '20', '10', '10', 'Milik Masyarakat', '', '', ''),
(38, 'SA38', '3271010', '3271010008', '1', '13', 'Mulyasari', '', '', '-6.67594', '106.84703', '40', '5', '35', 'Milik Masyarakat', '', '', ''),
(39, 'SA39', '3271010', '3271010008', '3', '9', 'Bakom Sari', '', '', '-6.66441', '106.84342', '70', '40', '30', 'Milik Masyarakat', '', '', ''),
(40, 'SA40', '3271010', '3271010003', '', '11', 'Lebak Wangi', '', '', '-6.6203', '106.80067', '30', '5', '25', 'Milik Masyarakat', '', '', ''),
(41, 'SA41', '3271010', '3271010005', '2', '8', 'Gg. Pala Margabakti', '', '', '-6.65161', '106.82332', '30', '8', '22', 'Milik Rancamaya', '', '', ''),
(42, 'SA42', '3271010', '3271010002', '2', '11', 'Pabuaran', '', '', '-6.64162', '106.79595', '50', '8', '42', 'Milik Masyarakat', '', '', ''),
(43, 'SA43', '3271010', '3271010001', '1', '11', 'Cibeureum', '', '', '-6.6512', '106.77822', '15', '8', '7', 'Hibah', '', '', ''),
(44, 'SA44', '3271010', '3271010001', '3', '4', 'Cibeureum', '', '', '-6.64543', '106.78723', '100', '40', '60', 'Milik Perumahan', '', '', ''),
(45, 'SA45', '3271010', '3271010001', '1', '1', 'Lemah Duhur', '', '', '-6.6544', '106.78057', '50', '8', '42', 'Milik Masyarakat', '', '', ''),
(46, 'SA46', '3271010', '3271010001', '5', '1', 'Lemah Duhur', '', '', '-6.65103', '106.78681', '50', '10', '40', 'Milik Masyarakat', '', '', ''),
(47, 'SA47', '3271010', '3271010016', '2', '7', 'Cikaret', '', '', '-6.61824', '106.78603', '20', '8', '12', 'Hibah', 'APBD', '2013', ''),
(48, 'SA48', '3271010', '3271010016', '2', '2', 'Gg. Kesadaran', '', '', '-6.61222', '106.78923', '40', '25', '15', 'Tanah Wakaf', 'APBD', '2009', ''),
(49, 'SA49', '3271010', '3271010016', '1', '1', 'Gg. Madrasah', '', '', '-6.6136', '106.78992', '30', '10.5', '19.5', 'Milik Masyarakat', 'APBN', '2014', ''),
(50, 'SA50', '3271010', '3271010016', '6', '4', 'Gg. Pangumbahan', '', '', '-6.61723', '106.7871', '20', '8', '12', 'Milik Masyarakat', '', '', ''),
(51, '', '3271010', '3271010006', '2', '5', 'Legok Muncang', '', '', '-6.67661', '106.82816', '110', '9', '101', 'Milik Masyarakat', 'APBD', '2015', ''),
(52, '', '3271010', '3271010006', '2', '5', 'Legok Muncang', '', '', '-6.67661', '106.82816', '110', '9', '101', 'Milik Masyarakat', 'APBD', '2015', ''),
(53, '', '3271010', '3271010006', '2', '5', 'Legok Muncang', '', '', '-6.67661', '106.82816', '110', '9', '101', 'Milik Masyarakat', 'APBD', '2015', ''),
(54, '', '3271010', '3271010006', '2', '5', 'Legok Muncang', '', '', '-6.67661', '106.82816', '110', '9', '101', 'Milik Masyarakat', 'APBD', '2015', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_potensi_pengembangan`
--

CREATE TABLE `t_potensi_pengembangan` (
  `id` int(11) NOT NULL,
  `id_sa` varchar(50) NOT NULL,
  `tdk_ada` varchar(50) DEFAULT NULL,
  `letak` varchar(50) NOT NULL,
  `luas` varchar(50) NOT NULL,
  `pemilik` varchar(50) NOT NULL,
  `minat_sikap_masyarakat` varchar(50) NOT NULL,
  `keberadaan_lembaga` varchar(50) NOT NULL,
  `keaktifan_lembaga` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_potensi_pengembangan`
--

INSERT INTO `t_potensi_pengembangan` (`id`, `id_sa`, `tdk_ada`, `letak`, `luas`, `pemilik`, `minat_sikap_masyarakat`, `keberadaan_lembaga`, `keaktifan_lembaga`) VALUES
(1, 'id_sa', 'tdk_ada', 'letak', 'luas', 'pemilik', 'minat_sikap_masyarakat', 'keberadaan_lembag', 'keaktidan_lembaga'),
(2, 'SA2', '-', '-', '-', '-', '-', '-', '-'),
(3, 'SA3', '-', '-', '-', '-', '-', '-', '-'),
(4, 'SA4', '-', '-', '-', '-', '-', '-', '-'),
(5, 'SA5', '-', '-', '-', '-', '-', '-', '-'),
(6, 'SA6', '-', '-', '-', '-', '-', '-', '-'),
(7, 'SA7', '-', '-', '-', '-', '-', '-', '-'),
(8, 'SA8', '-', '-', '-', '-', '-', '-', '-'),
(9, 'SA9', '-', '-', '-', '-', '-', '-', '-'),
(10, 'SA10', '-', '-', '-', '-', '-', '-', '-'),
(11, 'SA11', '-', '-', '-', '-', '-', '-', '-'),
(12, 'SA12', '-', '-', '-', '-', '-', '-', '-'),
(13, 'SA13', '-', '-', '-', '-', '-', '-', '-'),
(14, 'SA14', '-', '-', '-', '-', '-', '-', '-'),
(15, 'SA15', '-', '-', '-', '-', '-', '-', '-'),
(16, 'SA16', '-', '-', '-', '-', '-', '-', '-'),
(17, 'SA17', '-', '-', '-', '-', '-', '-', '-'),
(18, 'SA18', '-', '-', '-', '-', '-', '-', '-'),
(19, 'SA19', '-', '-', '-', '-', '-', '-', '-'),
(20, 'SA20', '-', '-', '-', '-', '-', '-', '-'),
(21, 'SA21', '-', '-', '-', '-', '-', '-', '-'),
(22, 'SA22', '-', '-', '-', '-', '-', '-', '-'),
(23, 'SA23', '-', '-', '-', '-', '-', '-', '-'),
(24, 'SA24', '-', '-', '-', '-', '-', '-', '-'),
(25, 'SA25', '-', '-', '-', '-', '-', '-', '-'),
(26, 'SA26', '-', '-', '-', '-', '-', '-', '-'),
(27, 'SA27', '-', '-', '-', '-', '-', '-', '-'),
(28, 'SA28', '-', '-', '-', '-', '-', '-', '-'),
(29, 'SA29', '-', '-', '-', '-', '-', '-', '-'),
(30, 'SA30', '-', '-', '-', '-', '-', '-', '-'),
(31, 'SA31', '-', '-', '-', '-', '-', '-', '-'),
(32, 'SA32', '-', '-', '-', '-', '-', '-', '-'),
(34, 'SA34', '-', '-', '-', '-', '-', '-', '-'),
(35, 'SA35', '-', '-', '-', '-', '-', '-', '-'),
(36, 'SA36', '-', '-', '-', '-', '-', 'Ada', 'Aktif, tidak rutin'),
(37, 'SA37', '-', '-', '-', '-', 'Biasa Aja', 'Ada', 'Aktif, tidak rutin'),
(38, 'SA38', '-', '-', '-', '-', 'Antusias', 'Tidak', 'Tidak Aktif'),
(39, 'SA39', '-', '-', '-', '-', 'Biasa Aja', 'Tidak', 'Tidak Aktif'),
(40, 'SA40', '-', '-', '-', '-', 'Biasa Aja', 'Tidak', '-'),
(41, 'SA41', '-', '-', '-', '-', '-', '-', '-'),
(42, 'SA42', '-', '-', '-', '-', '-', '-', '-'),
(43, 'SA43', '-', '-', '-', '-', 'Antusias', '-', '-'),
(44, 'SA44', '-', '-', '-', '-', '-', '-', '-'),
(45, 'SA45', '-', '-', '-', '-', 'Biasa Saja', '-', '-'),
(46, 'SA46', '-', '-', '-', '-', 'Biasa Saja', '-', '-'),
(47, 'SA47', '-', '-', '-', '-', 'Biasa Saja', '-', '-'),
(48, 'SA48', '-', '-', '-', '-', 'Biasa Saja', '-', '-'),
(49, 'SA49', '-', '-', '-', '-', 'Biasa Saja', '-', '-'),
(50, 'SA50', '-', '-', '-', '-', 'Antusias', 'Tidak', 'Tidak'),
(51, '', NULL, '-', '-', '-', '-', '-', '-'),
(52, '', NULL, '-', '-', '-', '-', '-', '-'),
(53, '', NULL, '-', '-', '-', '-', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `t_sistem_pengolahan`
--

CREATE TABLE `t_sistem_pengolahan` (
  `id` int(50) NOT NULL,
  `id_sa` varchar(50) NOT NULL,
  `sis_pengolahan` enum('ada','tidak ada') NOT NULL,
  `bangunan_pelengkap` text NOT NULL,
  `panjang` int(11) NOT NULL,
  `lebar` int(11) NOT NULL,
  `tinggi` int(11) NOT NULL,
  `jari_jari` int(11) NOT NULL,
  `kapasitas_terbangun` varchar(30) NOT NULL,
  `material_konstruksi` varchar(30) NOT NULL,
  `sistem_pemeliharaan` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `asbuilt_drawing_jaringan` enum('ada','tidak ada') NOT NULL,
  `peta_posisi_pelanggan` enum('ada','tidak ada') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_sistem_pengolahan`
--

INSERT INTO `t_sistem_pengolahan` (`id`, `id_sa`, `sis_pengolahan`, `bangunan_pelengkap`, `panjang`, `lebar`, `tinggi`, `jari_jari`, `kapasitas_terbangun`, `material_konstruksi`, `sistem_pemeliharaan`, `deskripsi`, `asbuilt_drawing_jaringan`, `peta_posisi_pelanggan`) VALUES
(1, 'id_sa', '', 'bangunan_pelengkap', 0, 0, 0, 0, 'kapasitas_terbangun', 'material_konstruksi', 'sistem_pemeliharaan', 'deskripsi', '', ''),
(2, 'SA2', 'tidak ada', 'Toilet dan Kamar mandi', 12, 3, 3, 0, '', 'Beton', 'Pembersihan berkala', 'pengelolaan dilakukan oleh kelompok ', 'tidak ada', 'tidak ada'),
(3, 'SA3', 'tidak ada', 'MCK', 5, 3, 3, 0, '', 'Beton', 'tidak ada', '', 'tidak ada', 'tidak ada'),
(4, 'SA4', 'tidak ada', '', 10, 5, 3, 0, '', 'Beton', 'tidak ada', 'air diperuntukan banyak untuk kepentingan mesjid baik berupa MCK, wudhu dll', 'tidak ada', 'tidak ada'),
(5, 'SA5', 'tidak ada', '', 6, 3, 2, 0, '', 'Beton', 'Pembersihan berkala', 'perumah memakai saringan', 'tidak ada', 'ada'),
(6, 'SA6', 'tidak ada', 'MCK', 5, 4, 4, 0, '', 'Beton', 'tidak ada', 'bangunan tidak terawat sama sekali', 'tidak ada', 'tidak ada'),
(7, 'SA7', 'tidak ada', '', 2, 2, 7, 0, '', 'Beton', 'tidak ada', 'Air tidak ada sama sekali', 'tidak ada', 'tidak ada'),
(8, 'SA8', 'tidak ada', '', 3, 3, 3, 0, '', 'Beton', 'tidak ada', 'berada di pinggir sungai cibogor', 'tidak ada', 'tidak ada'),
(9, 'SA9', '', '', 0, 0, 0, 0, '', '', '', '', '', ''),
(10, 'SA10', 'ada', 'MCK', 7, 3, 3, 0, '', 'beton', 'Pembersihan berkala', '2 minggu sekali pembersihan', 'tidak ada', 'tidak ada'),
(11, 'SA11', 'tidak ada', '', 0, 0, 0, 0, '', '', '', 'tidak ada pengelolaan', 'tidak ada', 'tidak ada'),
(12, 'SA12', 'tidak ada', '', 0, 0, 0, 0, '', '', '', 'tidak ada pengelolaan', '', ''),
(13, 'SA13', 'tidak ada', '', 7, 3, 4, 0, '', 'beton', 'lainnya', 'tidak ada perawatan , bangunan terlihat kotor dan air limbah MCK di aliri kesungai', 'tidak ada', 'tidak ada'),
(14, 'SA14', '', '', 3, 2, 3, 0, '', 'beton', '', 'bangunan tersedia dalam kondisi tidak terawat dan rusak. mata air tidak lagi mengeluarkan air', 'tidak ada', 'tidak ada'),
(15, 'SA15', 'tidak ada', '', 0, 0, 0, 0, '', '', '', '', 'tidak ada', 'tidak ada'),
(16, 'SA16', '', '', 0, 0, 0, 0, '', '', '', '', '', ''),
(17, 'SA17', 'tidak ada', '', 6, 4, 4, 0, '', 'beton', '', 'bangunan rusak berat', 'tidak ada', 'tidak ada'),
(18, 'SA18', '', '', 5, 4, 3, 0, '', 'beton', '', '', 'tidak ada', 'tidak ada'),
(19, 'SA19', '', '', 4, 3, 0, 0, '', '', '', '', '', ''),
(20, 'SA20', '', 'MCK', 4, 4, 3, 0, '', 'beton', '', '', '', ''),
(21, 'SA21', '', 'MCK', 6, 4, 2, 0, '', '', '', '', '', ''),
(22, 'SA22', '', 'MCK', 5, 5, 3, 0, '', 'beton', '', '', '', ''),
(23, 'SA23', 'tidak ada', 'MCK', 5, 3, 4, 0, '', 'beton', '', 'bangunan rusak', 'tidak ada', 'tidak ada'),
(24, 'SA24', 'tidak ada', '', 3, 2, 2, 0, '', 'beton', '', 'bangunan rusak', 'tidak ada', 'tidak ada'),
(25, 'SA25', 'tidak ada', 'MCK', 3, 2, 2, 0, '', 'beton', '', 'bangunan perlu di perbaiki', 'tidak ada', 'tidak ada'),
(26, 'SA26', 'tidak ada', 'MCK', 3, 2, 2, 0, '', 'beton', '', 'bangunan perlu di perbaiki', 'tidak ada', 'tidak ada'),
(27, 'SA27', 'tidak ada', 'MCK', 3, 2, 2, 0, '', 'beton', '', 'bangunan perlu di perbaiki', 'tidak ada', 'tidak ada'),
(28, 'SA28', 'tidak ada', 'MCK', 5, 3, 3, 0, '', 'beton', '', '', 'tidak ada', 'tidak ada'),
(29, 'SA29', '', '', 0, 0, 0, 0, '', '', '', '', '', ''),
(30, 'SA30', '', '', 0, 0, 0, 0, '', '', '', '', '', ''),
(31, 'SA31', '', 'MCK', 5, 2, 3, 0, '', 'beton', '', '', '', ''),
(32, 'SA32', 'ada', 'Bak Kolam Penampung', 3, 3, 6, 2, '', 'Beton', 'Tidak Ada', 'Mata Air di alirkan dan di tampungkan kedalam kolam penampung, dari kolam penampung di sedot dengan pipa dan simpan di toren, dan di alirkan melalui pipa. Sudah ada meteran setiap rumah', 'tidak ada', 'tidak ada'),
(34, 'SA34', 'tidak ada', 'Bak Penampung', 3, 2, 1, 0, '10 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(35, 'SA35', 'ada', 'Bak Penampung', 3, 3, 2, 0, '10 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(36, 'SA36', 'tidak ada', 'Bak Penampung', 3, 2, 1, 0, '20 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(37, 'SA37', 'ada', 'MCK', 3, 2, 1, 0, '10 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(38, 'SA38', 'ada', 'Bak Penampung', 3, 2, 1, 0, '11 KK', 'Beton', 'Tidak Ada', '', 'tidak ada', 'tidak ada'),
(39, 'SA39', 'ada', 'MCK', 2, 1, 3, 0, '10 KK', 'Beton', 'Tidak Ada', '', 'tidak ada', 'tidak ada'),
(40, 'SA40', 'ada', 'Bak Penampung', 1, 1, 1, 0, '20 KK', 'Beton', 'Tidak Ada', '', 'tidak ada', 'tidak ada'),
(41, 'SA41', 'ada', 'Bak Penampung', 3, 2, 1, 0, '15 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(42, 'SA42', 'ada', 'Bak Penampung', 1, 1, 2, 0, '14 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(43, 'SA43', 'ada', 'MCK', 4, 4, 2, 0, '', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(44, 'SA44', 'ada', 'Sudah Ada Intalasi Pengolahan Air', 4, 4, 2, 0, '', 'Beton', 'Pembersihan Rutin', 'Mata Air di alirkan dan di oleh dengan intalasi pengolahan air,  dan di alirkan melalui pipa. Sudah ada meteran setiap rumah', 'tidak ada', 'tidak ada'),
(45, 'SA45', 'ada', 'Toren Penampung Air', 4, 4, 2, 0, '50 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(46, 'SA46', 'tidak ada', 'MCK', 3, 1, 2, 0, '50 KK', 'Beton', 'Tidak Terawat', '', 'tidak ada', 'tidak ada'),
(47, 'SA47', 'tidak ada', 'MCK', 3, 3, 3, 0, '40 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(48, 'SA48', 'tidak ada', 'MCK', 5, 3, 1, 0, '20 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(49, 'SA49', 'tidak ada', 'MCK', 4, 1, 2, 0, '20 KK', 'Beton', 'Pembersihan Berkala', '', 'tidak ada', 'tidak ada'),
(50, 'SA50', 'tidak ada', '', 3, 1, 2, 0, '74 KK', '', '', 'Mata air keluar di tebing sungai', 'tidak ada', 'tidak ada'),
(51, '', 'ada', 'Bak Kolam Penampung', 3, 3, 6, 2, '', 'Beton', 'Tidak Ada', 'Mata Air di alirkan dan di tampungkan kedalam kolam penampung, dari kolam penampung di sedot dengan pipa dan simpan di toren, dan di alirkan melalui pipa. Sudah ada meteran setiap rumah', 'tidak ada', 'tidak ada'),
(52, '', 'ada', 'Bak Kolam Penampung', 3, 3, 6, 2, '', 'Beton', 'Tidak Ada', 'Mata Air di alirkan dan di tampungkan kedalam kolam penampung, dari kolam penampung di sedot dengan pipa dan simpan di toren, dan di alirkan melalui pipa. Sudah ada meteran setiap rumah', 'tidak ada', 'tidak ada'),
(53, '', 'ada', 'Bak Kolam Penampung', 3, 3, 6, 2, '', 'Beton', 'Tidak Ada', 'Mata Air di alirkan dan di tampungkan kedalam kolam penampung, dari kolam penampung di sedot dengan pipa dan simpan di toren, dan di alirkan melalui pipa. Sudah ada meteran setiap rumah', 'tidak ada', 'tidak ada'),
(54, '', 'ada', 'Bak Kolam Penampung', 3, 3, 6, 2, '', 'Beton', 'Tidak Ada', 'Mata Air di alirkan dan di tampungkan kedalam kolam penampung, dari kolam penampung di sedot dengan pipa dan simpan di toren, dan di alirkan melalui pipa. Sudah ada meteran setiap rumah', 'tidak ada', 'tidak ada');

-- --------------------------------------------------------

--
-- Table structure for table `t_sis_air_minum`
--

CREATE TABLE `t_sis_air_minum` (
  `id` int(50) NOT NULL,
  `id_sa` varchar(50) NOT NULL,
  `sumber_air` varchar(30) NOT NULL,
  `arah_mata_air` varchar(30) NOT NULL,
  `kedalaman` varchar(30) NOT NULL,
  `debit_liter` varchar(30) NOT NULL,
  `debit_detik` varchar(30) NOT NULL,
  `debit_air` varchar(30) NOT NULL,
  `pjg_pipa` varchar(30) NOT NULL,
  `kebersihan` enum('terawat','kurang terawat','tidak terawat') NOT NULL,
  `fungsi` enum('berfungsi','tidak berfungsi') NOT NULL,
  `kondisi` enum('baik','sebagian rusak','rusak') NOT NULL,
  `pemeliharaan` enum('ada dan rutin','ada dan tidak rutin','tidak ada') NOT NULL,
  `penjelasan_sam` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_sis_air_minum`
--

INSERT INTO `t_sis_air_minum` (`id`, `id_sa`, `sumber_air`, `arah_mata_air`, `kedalaman`, `debit_liter`, `debit_detik`, `debit_air`, `pjg_pipa`, `kebersihan`, `fungsi`, `kondisi`, `pemeliharaan`, `penjelasan_sam`) VALUES
(1, 'id_sa', 'sumber_air', 'arah_mata_air', 'kedalaman', 'debit_liter', 'debit_detik', 'debit_air', 'pjg_pipa', '', '', '', '', 'penjelasan'),
(2, 'SA2', 'Mata air', '140', '-', '3', '22.6', '0.13', '', 'terawat', 'berfungsi', 'baik', '', 'pemeliharaan dengan cara gotong royong sesama'),
(3, 'SA3', 'Mata air', '230', '-', '3', '19.21', '0.16', '', 'tidak terawat', 'berfungsi', 'sebagian rusak', 'tidak ada', 'bangunan tidak terawat dan kotor'),
(4, 'SA4', 'Mata air', '65', '-', '600', '9.3', '0.06', '', 'kurang terawat', 'berfungsi', 'rusak', 'tidak ada', 'karena pengelola bersifat individu.'),
(5, 'SA5', 'Air Tanah', '75', '-', '7500', '900', '8.33', '', 'terawat', 'berfungsi', 'sebagian rusak', '', 'pipa kadang tersumbat'),
(6, 'SA6', 'Air Tanah', '15', '44', '-', '-', '-', '1000', 'tidak terawat', 'tidak berfungsi', 'rusak', 'tidak ada', 'Air tidak ada'),
(7, 'SA7', 'Air Tanah', '110', '15', '-', '-', '-', '', 'tidak terawat', 'tidak berfungsi', 'rusak', 'tidak ada', 'Air tidak ada'),
(8, 'SA8', 'Mata air', '15', '-', '3', '5.97', '0.5', '', 'tidak terawat', 'berfungsi', 'sebagian rusak', 'tidak ada', ''),
(9, 'SA9', '', '15', '-', '4', '2.94', '1.36', '', '', '', '', '', ''),
(10, 'SA10', 'Mata air', '180', '-', '3', '9.63', '0.31', '', 'terawat', 'berfungsi', 'baik', '', 'pengelolaan 2 minggu sekali rutin'),
(11, 'SA11', 'Mata air', '135', '-', '600', '5.78', '0.1', '', '', '', '', '', 'tidak ada bangunan'),
(12, 'SA12', '', '210', '-', '600', '5.85', '0.1', '', '', '', '', '', 'tidak ada bangunan'),
(13, 'SA13', 'Mata air', '25', '-', '600', '20.6', '0.03', '', 'kurang terawat', 'berfungsi', 'baik', 'tidak ada', 'warga menggunakan untuk mencuci, MCK dll'),
(14, 'SA14', 'Mata air', '25', '-', '-', '-', '-', '', 'tidak terawat', 'tidak berfungsi', 'rusak', 'tidak ada', 'bangunan tidak berguna, mata air mati'),
(15, 'SA15', 'Mata air', '70', '-', '3', '14.45', '0.21', '', '', '', '', '', 'hanya bersifat distribusi air dari sumber menggunakan selang'),
(16, 'SA16', 'Mata air', '115', '-', '3', '10.36', '0.29', '', '', '', '', '', 'tidak ada bangunan'),
(17, 'SA17', 'Mata air', '30', '-', '-', '-', '10', '', 'tidak terawat', 'berfungsi', 'rusak', '', 'swadaya masyarakat'),
(18, 'SA18', 'Mata air', '40', '-', '-', '-', '10', '', 'terawat', 'berfungsi', 'baik', '', 'dibangun oleh DKM'),
(19, 'SA19', 'Mata air', '110', '-', '-', '-', '10', '', 'kurang terawat', 'berfungsi', 'sebagian rusak', '', ''),
(20, 'SA20', 'Mata air', '200', '-', '-', '-', '10', '', 'kurang terawat', 'berfungsi', 'sebagian rusak', '', ''),
(21, 'SA21', 'Mata air', '240', '-', '-', '-', '10', '', '', '', '', '', ''),
(22, 'SA22', 'Mata air', '175', '-', '-', '-', '10', '', 'terawat', 'berfungsi', 'baik', '', ''),
(23, 'SA23', 'Mata air', '120', '-', '0.6', '21.23', '0.03', '', 'tidak terawat', '', 'rusak', 'tidak ada', 'bangunan tidak terawat, mata air hanya dialirkan dengan pipa, digunakan untuk MCK oleh warga'),
(24, 'SA24', 'Mata air', '35', '-', '0.6', '15.19', '0.04', '', 'tidak terawat', '', 'sebagian rusak', 'tidak ada', 'bangunan tidak layak'),
(25, 'SA25', 'Mata air', '15', '-', '0.6', '9.66', '0.06', '', 'kurang terawat', 'berfungsi', 'sebagian rusak', '', 'tidak dikelola dengan baik'),
(26, 'SA26', 'Mata air', '40', '-', '0.6', '9.66', '0.06', '', 'kurang terawat', 'berfungsi', 'sebagian rusak', '', 'tidak dikelola dengan baik'),
(27, 'SA27', 'Mata air', '140', '-', '0.6', '9.66', '0.06', '', 'kurang terawat', 'berfungsi', 'sebagian rusak', '', 'tidak dikelola dengan baik'),
(28, 'SA28', 'Mata air', '165', '-', '0.6', '6.3', '0.1', '', 'kurang terawat', 'berfungsi', 'sebagian rusak', 'tidak ada', 'mesin rusak'),
(29, 'SA29', 'Mata air', '110', '-', '-', '-', '-', '', '', '', '', '', 'air tidak ada'),
(30, 'SA30', 'Mata air', '170', '-', '-', '-', '-', '', '', '', '', '', 'air tidak ada'),
(31, 'SA31', 'Mata air', '160', '-', '0.6', '5.4', '0.11', '', 'kurang terawat', '', 'sebagian rusak', 'tidak ada', ''),
(32, 'SA32', 'Mata Air', '65', '', '1', '60', '0.02', '', 'terawat', 'tidak berfungsi', 'baik', '', 'Sarana Eksisting  tidak berfungsi sehingga tidak bisa di manfaatkan oleh masyarakat di karenakan air tidak mengalir yang di sebabkan hilangnya mesin pompa dan kapasitas listrik yang tidak memadai.'),
(34, 'SA34', 'Mata Air', '150', '', '3', '3.52', '0.85', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(35, 'SA35', 'Mata Air', '280', '', '3', '33.8', '0.09', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(36, 'SA36', 'Mata Air', '15', '', '2', '5.75', '0.35', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(37, 'SA37', 'Mata Air', '260', '', '5', '60', '0.08', '', 'kurang terawat', 'berfungsi', 'baik', 'tidak ada', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(38, 'SA38', 'Mata Air', '30', '', '3', '13', '0.23', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(39, 'SA39', 'Mata Air', '190', '', '', '', '-', '', 'kurang terawat', 'berfungsi', 'sebagian rusak', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(40, 'SA40', 'Mata Air', '55', '', '1', '19', '0.05', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di alirkan ke setiap rumah melalui pipa'),
(41, 'SA41', 'Mata Air', '232', '', '3', '3.8', '0.79', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di alirkan ke setiap rumah melalui pipa'),
(42, 'SA42', 'Air Permukaan', '12', '', '', '', '-', '', 'kurang terawat', 'berfungsi', 'sebagian rusak', '', 'Air berasal dari irigasi sungai dan di alirkan oleh pipa di tampung dengan bak penampung, dan di alirkan kerumah warga'),
(43, 'SA43', 'Mata Air', '20', '', '3', '11', '0.27', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(44, 'SA44', 'Mata Air', '20', '', '', '', '8.33', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di olah dengan instalasi pengolahan air dan di alirkan ke setiap rumah melalui pipa'),
(45, 'SA45', 'Mata Air', '85', '', '', '', '', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di alirkan ke setiap rumah melalui pipa'),
(46, 'SA46', 'Mata Air', '130', '', '3', '25', '0.12', '', 'kurang terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(47, 'SA47', 'Mata Air', '', '', '', '', '-', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(48, 'SA48', 'Mata Air', '290', '', '', '', '-', '', 'kurang terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(49, 'SA49', 'Mata Air', '', '', '', '', '-', '', 'terawat', 'berfungsi', 'baik', '', 'Mata Air di tampung melalui bak penampung dan di manfaatkan oleh masyarakat secara komunal'),
(50, 'SA50', 'Mata Air', '120', '', '3', '21', '0.14', '', 'kurang terawat', 'berfungsi', 'baik', '', 'Mata Air muncul ke permukaan dan di manfaat oleh masyarakat secara komunal'),
(51, '', 'Mata Air', '65', '', '1', '60', '0.02', '', 'terawat', 'tidak berfungsi', 'baik', '', 'Sarana Eksisting  tidak berfungsi sehingga tidak bisa di manfaatkan oleh masyarakat di karenakan air tidak mengalir yang di sebabkan hilangnya mesin pompa dan kapasitas listrik yang tidak memadai.'),
(52, '', 'Mata Air', '65', '', '1', '60', '0.02', '', 'terawat', 'tidak berfungsi', 'baik', '', 'Sarana Eksisting  tidak berfungsi sehingga tidak bisa di manfaatkan oleh masyarakat di karenakan air tidak mengalir yang di sebabkan hilangnya mesin pompa dan kapasitas listrik yang tidak memadai.'),
(53, '', 'Mata Air', '65', '', '1', '60', '0.02', '', 'terawat', 'tidak berfungsi', 'baik', '', 'Sarana Eksisting  tidak berfungsi sehingga tidak bisa di manfaatkan oleh masyarakat di karenakan air tidak mengalir yang di sebabkan hilangnya mesin pompa dan kapasitas listrik yang tidak memadai.'),
(54, '', 'Mata Air', '65', '', '1', '60', '0.02', '', 'terawat', 'tidak berfungsi', 'baik', '', 'Sarana Eksisting  tidak berfungsi sehingga tidak bisa di manfaatkan oleh masyarakat di karenakan air tidak mengalir yang di sebabkan hilangnya mesin pompa dan kapasitas listrik yang tidak memadai.');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` int(15) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_ekonomi`
--
ALTER TABLE `t_ekonomi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_gambaran_daerah_pelayanan`
--
ALTER TABLE `t_gambaran_daerah_pelayanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jaringan_pipa`
--
ALTER TABLE `t_jaringan_pipa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kecamatan`
--
ALTER TABLE `t_kecamatan`
  ADD PRIMARY KEY (`id_kecamatan`);

--
-- Indexes for table `t_kelurahan`
--
ALTER TABLE `t_kelurahan`
  ADD PRIMARY KEY (`id_desa`);

--
-- Indexes for table `t_lokasi`
--
ALTER TABLE `t_lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_potensi_pengembangan`
--
ALTER TABLE `t_potensi_pengembangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_sistem_pengolahan`
--
ALTER TABLE `t_sistem_pengolahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_sis_air_minum`
--
ALTER TABLE `t_sis_air_minum`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_ekonomi`
--
ALTER TABLE `t_ekonomi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `t_gambaran_daerah_pelayanan`
--
ALTER TABLE `t_gambaran_daerah_pelayanan`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `t_jaringan_pipa`
--
ALTER TABLE `t_jaringan_pipa`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `t_lokasi`
--
ALTER TABLE `t_lokasi`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `t_potensi_pengembangan`
--
ALTER TABLE `t_potensi_pengembangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `t_sistem_pengolahan`
--
ALTER TABLE `t_sistem_pengolahan`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `t_sis_air_minum`
--
ALTER TABLE `t_sis_air_minum`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id_user` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
