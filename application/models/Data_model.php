<?php
class Data_model extends CI_Model {

	function get_login($data)
	{
		$this->db->where($data);
		return $this->db->get('t_user');
	}

	function get_kecamatan()
	{
		return $this->db->get('t_kecamatan');
	}

	function kelurahan($id_kecamatan)
	{
		$this->db->where('kecamatan_id',$id_kecamatan);
		return $this->db->get('t_kelurahan');
	}

	function count_record()
	{
		return $this->db->get('t_lokasi')->num_rows()+1;
	}

	function input_lokasi($data)
	{
		$this->db->insert('t_lokasi',$data);
	}

	function input_sis_air_minum($data)
	{
		$this->db->insert('t_sis_air_minum',$data);
	}

	function input_jar_pipa($data)
	{
		$this->db->insert('t_jaringan_pipa',$data);
	}

	function input_sis_pengolahan($data)
	{
		$this->db->insert('t_sistem_pengolahan',$data);
	}

	function input_ekonomi($data)
	{
		$this->db->insert('t_ekonomi',$data);
	}

	function input_gbr_daerah($data)
	{
		$this->db->insert('t_gambaran_daerah_pelayanan',$data);
	}

	function input_potensi($data)
	{
		$this->db->insert('t_potensi_pengembangan',$data);
	}

	function get_data_sumber_air()
	{
		$this->db->join('t_kecamatan','t_lokasi.kecamatan = t_kecamatan.id_kecamatan');
		$this->db->join('t_kelurahan','t_lokasi.kelurahan = t_kelurahan.id_desa');
		return $this->db->get('t_lokasi');
	}

	function get_detail_information($id_sa)
	{
		$this->db->join('t_sis_air_minum','t_sis_air_minum.id_sa = t_lokasi.id_sa');
		$this->db->join('t_sistem_pengolahan','t_sistem_pengolahan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_potensi_pengembangan','t_potensi_pengembangan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_jaringan_pipa','t_jaringan_pipa.id_sa = t_lokasi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan','t_gambaran_daerah_pelayanan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_ekonomi','t_ekonomi.id_sa = t_lokasi.id_sa');
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');

		$this->db->where('t_lokasi.id_sa',$id_sa);
		return $this->db->get('t_lokasi');
	}

	function get_filter_data($where)
	{
		$this->db->join('t_sis_air_minum','t_sis_air_minum.id_sa = t_lokasi.id_sa');
		$this->db->join('t_sistem_pengolahan','t_sistem_pengolahan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_potensi_pengembangan','t_potensi_pengembangan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_jaringan_pipa','t_jaringan_pipa.id_sa = t_lokasi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan','t_gambaran_daerah_pelayanan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_ekonomi','t_ekonomi.id_sa = t_lokasi.id_sa');
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');

		$this->db->where($where);
		return $this->db->get('t_lokasi');
	}

	function get_filter_data2()
	{
		$this->db->join('t_sis_air_minum','t_sis_air_minum.id_sa = t_lokasi.id_sa');
		$this->db->join('t_sistem_pengolahan','t_sistem_pengolahan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_potensi_pengembangan','t_potensi_pengembangan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_jaringan_pipa','t_jaringan_pipa.id_sa = t_lokasi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan','t_gambaran_daerah_pelayanan.id_sa = t_lokasi.id_sa');
		$this->db->join('t_ekonomi','t_ekonomi.id_sa = t_lokasi.id_sa');
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');

		return $this->db->get('t_lokasi');
	}

	function edit_lokasi($data,$id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->set($data);
		$this->db->update('t_lokasi');
	}

	function edit_sis_air_minum($data,$id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->set($data);
		$this->db->update('t_sis_air_minum');
	}

	function edit_jar_pipa($data,$id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->set($data);
		$this->db->update('t_jaringan_pipa');
	}

	function edit_sis_pengolahan($data,$id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->set($data);
		$this->db->update('t_sistem_pengolahan');
	}

	function edit_ekonomi($data,$id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->set($data);
		$this->db->update('t_ekonomi');
	}

	function edit_gbr_daerah($data,$id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->set($data);
		$this->db->update('t_gambaran_daerah_pelayanan');
	}

	function edit_potensi($data,$id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->set($data);
		$this->db->update('t_potensi_pengembangan');
	}

	function get_map_marker_b_selatan()
	{
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');
		$this->db->join('t_sis_air_minum','t_lokasi.id_sa = t_sis_air_minum.id_sa');
		$this->db->join('t_jaringan_pipa', 't_lokasi.id_sa = t_jaringan_pipa.id_sa');
		$this->db->join('t_sistem_pengolahan', 't_lokasi.id_sa = t_sistem_pengolahan.id_sa');
		$this->db->join('t_ekonomi', 't_lokasi.id_sa = t_ekonomi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan', 't_lokasi.id_sa = t_gambaran_daerah_pelayanan.id_sa');

		//$this->db->join('t_potensi_pengembangan', 't_lokasi.id_sa = t_potensi_pengembangan.id_sa');

		$this->db->where('kecamatan','3271010');
		return $this->db->get('t_lokasi');
	}

	function get_map_marker_b_timur()
	{
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');
		$this->db->join('t_sis_air_minum','t_lokasi.id_sa = t_sis_air_minum.id_sa');
		$this->db->join('t_jaringan_pipa', 't_lokasi.id_sa = t_jaringan_pipa.id_sa');
		$this->db->join('t_sistem_pengolahan', 't_lokasi.id_sa = t_sistem_pengolahan.id_sa');
		$this->db->join('t_ekonomi', 't_lokasi.id_sa = t_ekonomi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan', 't_lokasi.id_sa = t_gambaran_daerah_pelayanan.id_sa');

		//$this->db->join('t_potensi_pengembangan', 't_lokasi.id_sa = t_potensi_pengembangan.id_sa');
		$this->db->where('kecamatan','3271020');
		return $this->db->get('t_lokasi');
	}

	function get_map_marker_b_utara()
	{
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');
		$this->db->join('t_sis_air_minum','t_lokasi.id_sa = t_sis_air_minum.id_sa');
		$this->db->join('t_jaringan_pipa', 't_lokasi.id_sa = t_jaringan_pipa.id_sa');
		$this->db->join('t_sistem_pengolahan', 't_lokasi.id_sa = t_sistem_pengolahan.id_sa');
		$this->db->join('t_ekonomi', 't_lokasi.id_sa = t_ekonomi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan', 't_lokasi.id_sa = t_gambaran_daerah_pelayanan.id_sa');

		//$this->db->join('t_potensi_pengembangan', 't_lokasi.id_sa = t_potensi_pengembangan.id_sa');
		$this->db->where('kecamatan','3271030');
		return $this->db->get('t_lokasi');
	}

	function get_map_marker_b_tengah()
	{
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');
		$this->db->join('t_sis_air_minum','t_lokasi.id_sa = t_sis_air_minum.id_sa');
		$this->db->join('t_jaringan_pipa', 't_lokasi.id_sa = t_jaringan_pipa.id_sa');
		$this->db->join('t_sistem_pengolahan', 't_lokasi.id_sa = t_sistem_pengolahan.id_sa');
		$this->db->join('t_ekonomi', 't_lokasi.id_sa = t_ekonomi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan', 't_lokasi.id_sa = t_gambaran_daerah_pelayanan.id_sa');

		//$this->db->join('t_potensi_pengembangan', 't_lokasi.id_sa = t_potensi_pengembangan.id_sa');
		$this->db->where('kecamatan','3271040');
		return $this->db->get('t_lokasi');
	}
	function get_map_marker_b_barat()
	{
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');
		$this->db->join('t_sis_air_minum','t_lokasi.id_sa = t_sis_air_minum.id_sa');
		$this->db->join('t_jaringan_pipa', 't_lokasi.id_sa = t_jaringan_pipa.id_sa');
		$this->db->join('t_sistem_pengolahan', 't_lokasi.id_sa = t_sistem_pengolahan.id_sa');
		$this->db->join('t_ekonomi', 't_lokasi.id_sa = t_ekonomi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan', 't_lokasi.id_sa = t_gambaran_daerah_pelayanan.id_sa');

		//$this->db->join('t_potensi_pengembangan', 't_lokasi.id_sa = t_potensi_pengembangan.id_sa');
		$this->db->where('kecamatan','3271050');
		return $this->db->get('t_lokasi');
	}
	function get_map_marker_tan_sareal()
	{
		$this->db->join('t_kecamatan','t_kecamatan.id_kecamatan = t_lokasi.kecamatan');
		$this->db->join('t_kelurahan','t_kelurahan.id_desa = t_lokasi.kelurahan');
		$this->db->join('t_sis_air_minum','t_lokasi.id_sa = t_sis_air_minum.id_sa');
		$this->db->join('t_jaringan_pipa', 't_lokasi.id_sa = t_jaringan_pipa.id_sa');
		$this->db->join('t_sistem_pengolahan', 't_lokasi.id_sa = t_sistem_pengolahan.id_sa');
		$this->db->join('t_ekonomi', 't_lokasi.id_sa = t_ekonomi.id_sa');
		$this->db->join('t_gambaran_daerah_pelayanan', 't_lokasi.id_sa = t_gambaran_daerah_pelayanan.id_sa');

		//$this->db->join('t_potensi_pengembangan', 't_lokasi.id_sa = t_potensi_pengembangan.id_sa');
		$this->db->where('kecamatan','3271060');
		return $this->db->get('t_lokasi');
	}

	function hapus_data_t_lokasi($id_sa)
	{
		$this->db->where('id_sa',$id_sa);

		$this->db->delete('t_lokasi');
	}

	function hapus_data_t_sis_air_minum($id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->delete('t_sis_air_minum');
	}

	function hapus_data_t_sistem_pengolahan($id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->delete('t_sistem_pengolahan');
	}

	function hapus_data_t_potensi_pengembangan($id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->delete('t_potensi_pengembangan');
	}

	function hapus_data_t_jaringan_pipa($id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->delete('t_jaringan_pipa');
	}

	function hapus_data_t_ekonomi($id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->delete('t_ekonomi');
	}

	function hapus_data_t_gambaran_daerah_pelayanan($id_sa)
	{
		$this->db->where('id_sa',$id_sa);
		$this->db->delete('t_gambaran_daerah_pelayanan');
	}

	function get_user($id)
	{
		$this->db->where('id_user',$id);
		return $this->db->get('t_user');
	}
}
