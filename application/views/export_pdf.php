<!DOCTYPE html>
<html>
<head>
	<title>Preview</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="shortcut icon" href="<?php echo base_url('upload'); ?>/logo_bogor.png" />
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body>
	<style type="text/css">
		.header {
			text-align: center;
		}
	</style>
	<!-- CSS goes in the document HEAD or added to your external stylesheet -->
	<style type="text/css">
		table.gridtable {
			font-family: verdana,arial,sans-serif;
			font-size:11px;
			color:#333333;
			border-width: 1px;
			border-color: #666666;
			border-collapse: collapse;
			width: 100%;
		}
		table.gridtable th {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #dedede;
		}
		table.gridtable td {
			border-width: 1px;
			padding: 8px;
			border-style: solid;
			border-color: #666666;
			background-color: #ffffff;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header">
					<p>
						Data Sumber Air <br>
					</p>
				</div>
				<br>

				<table class="gridtable">
					<thead>
						<tr>
							<th>No.</th>
							<th>ID</th>
							<th>Kecamatan</th>
							<th>Kelurahan</th>
							<th>Alamat</th>
							<th>RT</th>
							<th>RW</th>
							<th>No. SK Legalitas Hibah</th>
							<th>No. SK Kelurahan</th>
							<th>Longitude</th>
							<th>Latitude</th>
							<th>Luas Lahan</th>
							<th>Luas Lahan Terbangun</th>
							<th>Perkiraan Sisa Lahan</th>
							<th>Status Lahan</th>
							<th>Pendanaan</th>
							<th>Tahun Pembangunan</th>
							<th>Sumber Air</th>
							<th>Arah Mata Air</th>
							<th>Kedalaman</th>
							<th>Debit Liter Air</th>
							<th>Debit Detik Air</th>
							<th>Debit Air (Liter/Detik)</th>
							<th>Panjang Pipa</th>
							<th>Kebersihan</th>
							<th>Fungsi</th>
							<th>Kondisi</th>
							<th>Pemeliharaan</th>
							<th>Penjelasan</th>
							<th>Jumlah SR Terpasang</th>
							<th>Kondisi Kelancaran Sistem</th>
							<th>Kondisi Perawatan Sistem</th>
							<th>Kondisi Fungsi Sistem</th>
							<th>Ketersediaan Hidran</th>
							<th>Jumlah Hidran</th>
							<th>Meteran</th>
							<th>Jumlah Meteran</th>
							<th>Pemeliharaan Sistem Jaringan</th>
							<th>Penjelasan</th>
							<th>Lebar Jalan</th>
							<th>Sistem Pengolahan yg digunakan</th>
							<th>Bangunan Pelengkap Lainnya</th>
							<th>Panjang</th>
							<th>Lebar</th>
							<th>Tinggi</th>
							<th>Jati-jari</th>
							<th>Kapasitas Terbangun</th>
							<th>Material Konstruksi</th>	
							<th>Sistem Pemeliharaan</th>
							<th>Deskripsi</th>
							<th>Asbuit Drawing Jaringan Perpipaan</th>
							<th>Peta Posisi Pelanggan</th>
							<th>Keberadaan Iuran</th>
							<th>Besaran</th>
							<th>Minat Utk Peningkatan Pelayanan</th>
							<th>Peningkatan Retribusi</th>
							<th>Besaran</th>
							<th>Gambaran Umum Daerah Pelayanan</th>
							<th>Hambatan Teknis</th>
							<th>Hambatan Non Teknis</th>
							<th>Potensi Pengembangan</th>
							<th>Letak</th>
							<th>Luas</th>
							<th>Pemilik</th>
							<th>Minat dan sikap masyarakat</th>
							<th>Keberadaan Kelembagaaan</th>
							<th>Keaktifan Kelembagaan</th>
						</tr>
					</thead>
					<tbody>
					<?php $no = 1; ?>
						<?php foreach ($query->result() as $row): ?>
							<tr>
								<td><?php echo $no; ?></td>
								<td><?php echo $row->id_sa; ?></td>
								<td><?php echo $row->nm_kecamatan; ?></td>
								<td><?php echo $row->nm_desa; ?></td>
								<td><?php echo $row->almt; ?></td>
								<td><?php echo $row->rt; ?></td>
								<td><?php echo $row->rw; ?></td>
								<td><?php echo $row->no_sk_lh; ?></td>
								<td><?php echo $row->no_sk_kel; ?></td>
								<td><?php echo $row->longitude; ?></td>
								<td><?php echo $row->latitude; ?></td>
								<td><?php echo $row->ls_lahan; ?></td>
								<td><?php echo $row->ls_lahan_terbangun; ?></td>
								<td><?php echo $row->perkiraan_sisa_lhn; ?></td>
								<td><?php echo $row->status_lahan; ?></td>
								<td><?php echo $row->pendanaan; ?></td>
								<td><?php echo $row->thn_pembangunan; ?></td>
								<td><?php echo $row->sumber_air; ?></td>
								<td><?php echo $row->arah_mata_air; ?></td>
								<td><?php echo $row->kedalaman; ?></td>
								<td><?php echo $row->debit_liter; ?></td>
								<td><?php echo $row->debit_detik; ?></td>
								<td><?php echo $row->debit_air; ?></td>
								<td><?php echo $row->pjg_pipa; ?></td>
								<td><?php echo $row->kebersihan; ?></td>
								<td><?php echo $row->fungsi; ?></td>
								<td><?php echo $row->kondisi; ?></td>
								<td><?php echo $row->pemeliharaan; ?></td>
								<td><?php echo $row->penjelasan_sam; ?></td>
								<td><?php echo $row->jml_sr_terpasang; ?></td>
								<td><?php echo $row->kondisi_kelancaran_sis_pengalir; ?></td>
								<td><?php echo $row->kondisi_perawatan_sis_pengalir; ?></td>
								<td><?php echo $row->kondisi_fungsi_sis_pengalir; ?></td>
								<td><?php echo $row->ketersediaan_hidran; ?></td>
								<td><?php echo $row->jml_hidran; ?></td>
								<td><?php echo $row->meteran; ?></td>
								<td><?php echo $row->jml_meteran; ?></td>
								<td><?php echo $row->pemeliharaan_sis_jaringan; ?></td>
								<td><?php echo $row->penjelasan_jar_pipa; ?></td>
								<td><?php echo $row->lebar_jln; ?></td>
								<td><?php echo $row->sis_pengolahan; ?></td>
								<td><?php echo $row->bangunan_pelengkap; ?></td>
								<td><?php echo $row->panjang; ?></td>
								<td><?php echo $row->lebar; ?></td>
								<td><?php echo $row->tinggi; ?></td>
								<td><?php echo $row->jari_jari; ?></td>
								<td><?php echo $row->kapasitas_terbangun; ?></td>
								<td><?php echo $row->material_konstruksi; ?></td>
								<td><?php echo $row->sistem_pemeliharaan; ?></td>
								<td><?php echo $row->deskripsi; ?></td>
								<td><?php echo $row->asbuilt_drawing_jaringan; ?></td>
								<td><?php echo $row->peta_posisi_pelanggan; ?></td>
								<td><?php echo $row->keberadaan_iuran; ?></td>
								<td><?php echo $row->besaran_tarif; ?></td>
								<td><?php echo $row->minat_utk_peningkat; ?></td>
								<td><?php echo $row->peningkatan_retribusi; ?></td>
								<td><?php echo $row->besaran_retribusi; ?></td>
								<td><?php echo $row->gbr_umum; ?></td>
								<td><?php echo $row->hambatan_teknis; ?></td>
								<td><?php echo $row->hambatan_non_teknis; ?></td>
								<td><?php echo $row->tdk_ada; ?></td>
								<td><?php echo $row->letak; ?></td>
								<td><?php echo $row->luas; ?></td>
								<td><?php echo $row->pemilik; ?></td>
								<td><?php echo $row->minat_sikap_masyarakat; ?></td>
								<td><?php echo $row->keberadaan_lembaga; ?></td>
								<td><?php echo $row->keaktifan_lembaga; ?></td>
								<?php $no++; ?>
							</tr>
						<?php endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>	