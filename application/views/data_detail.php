<?php foreach($detail->result() as $row){} ?>
<?php include 'template/header.php' ?>
<style>


</style>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="header">
                        <h4 class="title">Informasi Detail Sumber Air</h4>
                    </div>
					<div class="content">
						<ul class="nav nav-tabs" role="tablist">
						  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-home"></i>  <span>Lokasi</span></a></li>
						  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><i class="fa fa-user"></i>  <span>Sistem Air Minum</span></a></li>
						  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><i class="fa fa-envelope-o"></i>  <span>Jaringan Pipa</span></a></li>
						  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-cog"></i>  <span>Sistem Pengolahan</span></a></li>
						  <li role="presentation"><a href="#extra" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-plus-square-o"></i>  <span>Ekonomi</span></a></li>
						  <li role="presentation"><a href="#gbr_umum" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-plus-square-o"></i>  <span>Gbr. Daerah Pelayanan</span></a></li>
						  <li role="presentation"><a href="#potensi" aria-controls="settings" role="tab" data-toggle="tab"><i class="fa fa-plus-square-o"></i>  <span>Potensi</span></a></li>
						</ul>
						
						<!-- Tab panes -->
						<form action="<?php echo base_url('admin/proses_input_data'); ?>" method="post">
						<div class="tab-content">
						  <div role="tabpanel" class="tab-pane active" id="home">
							<div class="row">
								<!--<div class="col-md-12"><h4>Input Lokasi</h4></div>-->
								<div class="col-md-4">
									<div class="form-group">
										<label><strong>Kecamatan</strong> : <?php echo $row->nm_kecamatan; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>Kelurahan</strong> : <?php echo $row->nm_desa; ?></label>
									</div>
									<div class="form-group">
										<label><strong>RW</strong> : <?php echo $row->rw; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>RT</strong> : <?php echo $row->rt; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>Alamat</strong> : <?php echo $row->almt; ?></label>
										
									</div>
									
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label><strong>No. SK legalitas Hibah</strong> : <?php echo $row->no_sk_lh; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>No. SK Kelurahan</strong> : <?php echo $row->no_sk_kel; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>Luas Lahan</strong> : <?php echo $row->ls_lahan; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>Luas Lahan terbangun</strong> : <?php echo $row->ls_lahan_terbangun; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>Perkiraan Sisa Lahan</strong> : <?php echo $row->perkiraan_sisa_lhn; ?></label>
										
									</div>
									
								</div>
								<div class="col-md-4">
									
									<div class="form-group">
										<label><strong>Status Lahan</strong> : <?php echo $row->status_lahan; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Pendanaan</strong> : <?php echo $row->pendanaan; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Tahun Pembangunan</strong> : <?php echo $row->thn_pembangunan; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Longitude</strong> : <?php echo $row->longitude; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Latitude</strong> : <?php echo $row->latitude; ?></label>
									</div>
									<div class="form-group" align="right">
										<br>
										<!--<a href="<?php echo base_url('admin/edit_lokasi'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-warning"role="tab" data-toggle="tab">Edit</a>-->
										<a href="#profile" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="profile">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><strong>Sumber Air</strong> : <?php echo $row->sumber_air; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>Arah Mata Air</strong> : <?php echo $row->arah_mata_air; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Kedalaman</strong> : <?php echo $row->kedalaman; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Debit Liter</strong> : <?php echo $row->debit_liter; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Debit Detik</strong> : <?php echo $row->debit_detik; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Debit Air</strong> : <?php echo $row->debit_air; ?></label>
									</div>
									
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label><strong>Panjang Pipa</strong> : <?php echo $row->pjg_pipa; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Kebersihan</strong> : <?php echo $row->kebersihan; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Fungsi</strong> : <?php echo $row->fungsi; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Kondisi</strong> : <?php echo $row->kondisi; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Pemeliharaan</strong> : <?php echo $row->pemeliharaan; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Penjelasan</strong> : <?php echo $row->penjelasan_sam; ?></label>
									</div>
									<div class="form-group" align="right">
										<br>
										<!--<a href="<?php echo base_url('admin/edit_sis_air_minum'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-warning"role="tab" data-toggle="tab">Edit</a>-->
										<a href="#messages" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="messages">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
								<label><strong>Jumlah SR Terpasang</strong> : <?php echo $row->jml_sr_terpasang; ?></label>
							</div>
							<div class="form-group">
								<label><strong>Kondisi Kelancaran Sistem Pengalir</strong> : <?php echo $row->kondisi_kelancaran_sis_pengalir; ?></label>
							</div>
							<div class="form-group">
								<label><strong>Kondisi Perawatan Sistem Pengalir</strong> : <?php echo $row->kondisi_perawatan_sis_pengalir; ?></label>
							</div>
							<div class="form-group">
								<label><strong>Kondisi Fungsi Sistem Pengalir</strong> : <?php echo $row->kondisi_fungsi_sis_pengalir; ?></label>
							</div>
							<div class="form-group">
								<label><strong>Ketersediaan Hidran</strong> : <?php echo $row->ketersediaan_hidran; ?></label>
								
								<div class="form-group">
										<label><strong>Jumlah Hidran</strong> : <?php echo $row->jml_hidran; ?></label>
									</div>
							</div>
								</div>
								<div class="col-md-6">
									
									<div class="form-group">
										<label><strong>Meteran</strong> : <?php echo $row->meteran; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Jumlah Meteran</strong> : <?php echo $row->jml_meteran; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Pemeliharaan Sistem Jaringan</strong> : <?php echo $row->pemeliharaan_sis_jaringan; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>Penjelasan</strong> : <?php echo $row->penjelasan_jar_pipa; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Lebar Jalan</strong> : <?php echo $row->lebar_jln; ?></label>
									</div>
									<div class="form-group" align="right">
										<br>
										<!--<a href="<?php echo base_url('admin/edit_jaringan_pipa'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-warning"role="tab" data-toggle="tab">Edit</a>-->
										<a href="#settings" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="settings">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><strong>Sistem Pengolahan</strong> : <?php echo $row->sis_pengolahan; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Bangunan Pelengkap</strong> : <?php echo $row->bangunan_pelengkap; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Panjang</strong> : <?php echo $row->panjang; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Lebar</strong> : <?php echo $row->lebar; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Tinggi</strong> : <?php echo $row->tinggi; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Jari-jari</strong> : <?php echo $row->jari_jari; ?></label>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label><strong>Kapasitas Terbangun</strong> : <?php echo $row->kapasitas_terbangun; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Material Konstruksi</strong> : <?php echo $row->material_konstruksi; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Sistem Pemeliharaan</strong> : <?php echo $row->sistem_pemeliharaan; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Deskripsi</strong> : <?php echo $row->deskripsi; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Asbuilt Drawing Jaringan</strong> : <?php echo $row->asbuilt_drawing_jaringan; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Peta Posisi Pelanggan</strong> : <?php echo $row->peta_posisi_pelanggan; ?></label>
									</div>
									<div class="form-group" align="right">
										<br>
										<!--<a href="<?php echo base_url('admin/edit_sis_pengolahan'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-warning"role="tab" data-toggle="tab">Edit</a>-->
										<a href="#extra" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="extra">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><strong>Keberadaan Iuran</strong> : <?php echo $row->keberadaan_iuran; ?></label>
										
									</div>
									<div class="form-group">
										<label><strong>Besaran Tarif</strong> : <?php echo $row->besaran_tarif; ?></label> 
									</div>
									<div class="form-group">
										<label><strong>Minat Untuk Peningkat</strong> : <?php echo $row->minat_utk_peningkat; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Peningkatan Retribusi</strong>: <?php echo $row->peningkatan_retribusi; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Besaran Retribusi</strong> : <?php echo $row->besaran_retribusi; ?></label>
									</div>
									<div class="form-group" align="right">
										<br>
										<!--<a href="<?php echo base_url('admin/edit_ekonomi'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-warning"role="tab" data-toggle="tab">Edit</a>-->
										<a href="#gbr_umum" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
								<div class="col-md-6">
									
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="gbr_umum">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><strong>Gambar Umum</strong> : <?php echo $row->gbr_umum; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Hambatan Teknis</strong> : <?php echo $row->hambatan_teknis; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Hambatan Non Teknis</strong> : <?php echo $row->hambatan_non_teknis; ?></label>
									</div>
									<div class="form-group" align="right">
										<br>
										<!--<a href="<?php echo base_url('admin/edit_gbr_daerah'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-warning"role="tab" data-toggle="tab">Edit</a>-->
										<a href="#potensi" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
								<div class="col-md-6">
								
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="potensi">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label><strong>Potensi Pengembangan</strong> : <?php echo $row->tdk_ada; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Letak</strong> : <?php echo $row->letak;; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Luas</strong> : <?php echo $row->luas; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Pemilik</strong> : <?php echo $row->pemilik; ?></label> 
									</div>
									
								</div>
								
								<div class="col-md-6">
									<div class="form-group">
										<label><strong>Minat Sikap Masyarakat</strong> : <?php echo $row->minat_sikap_masyarakat; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Keberadaan Lembaga</strong> : <?php echo $row->keberadaan_lembaga; ?></label>
									</div>
									<div class="form-group">
										<label><strong>Keaktifan Kelembagaan</strong> : <?php echo $row->keaktifan_lembaga; ?></label> 
									</div>
									<div align="right">
										<!--<a href="<?php echo base_url('admin/edit_potensi'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-warning"role="tab" data-toggle="tab">Edit</a>-->
										<a class="btn btn-primary" href="<?php echo base_url('admin/sumber_air'); ?>">Done</a>
									</div>
								<div>
							</div>
						  </div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'template/footer.php' ?>
<script>
	$("#kecamatan").change(function(){
		var id_kecamatan = $("#kecamatan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('admin/kelurahan')?>",
			type: "POST",
			data	: "id_kecamatan="+id_kecamatan,
			success : function (msg) {
				document.getElementById("kelurahan").disabled = false;
				$("#kelurahan").html(msg);
				//$("#kelurahan").css("color","black");
			}
		});
	});
	$("#kelurahan").change(function(){
		var id_kelurahan = $("#kelurahan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('admin/rw')?>",
			type: "POST",
			data	: "id_kelurahan="+id_kelurahan,
			success : function (msg) {
				if(msg == 'kosong'){
					alert("Maaf data RW dari kelurahan yang dipilih belum tersedia, silahkan ganti pilihan kelurahan lain");
				}else{
					document.getElementById("rw").disabled = false;
					$("#rw").html(msg);
				}
			}
		});
	});
	$("#rw").change(function(){
		var rw = $("#rw option:selected").val();
		$.ajax({
			url: "<?php echo site_url('admin/rt')?>",
			type: "POST",
			data	: "no_rw="+rw,
			success : function (msg) {
				if(msg == 'kosong'){
					alert("Maaf data RT dari RW yang dipilih belum tersedia, silahkan ganti pilihan RW lain");
				}else{
					$("#rt").html(msg);
					document.getElementById("rt").disabled = false;
				}
			}
		});
	});
</script>