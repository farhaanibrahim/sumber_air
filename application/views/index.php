<?php include 'template/header.php'; ?>
	<style>
		#menu {
			background: #fff;
			position: absolute;
			z-index: 1;
			top: 120px;
			right: 30px;
			border-radius: 3px;
			width: 150px;
			font-family: 'Open Sans', sans-serif;
		}

		#menu a {
			font-size: 13px;
			color: #404040;
			display: block;
			margin: 0;
			padding: 0;
			padding: 10px;
			text-decoration: none;
			border-bottom: 1px solid rgba(0,0,0,0.25);
			text-align: center;
		}

		#menu a:last-child {
			border: none;
		}

		#menu a:hover {
			background-color: #f8f8f8;
			color: #404040;
		}

		#menu a.active {
			background-color: #3887be;
			color: #ffffff;
		}

		#menu a.active:hover {
			background: #3074a4;
		}

	</style>
    <div id="map"></div>
		<div id="menu"></div>
		<script>
			mapboxgl.accessToken = 'pk.eyJ1IjoiZmFyaGFhbmlicmFoaW0iLCJhIjoiY2owdGpxam1sMDVoNjJxbzVuZ3ZkeGs3diJ9.HoswZ_11luzV16HQ1CYONA';
            var map = new mapboxgl.Map({
								container: 'map',
                style: 'mapbox://styles/mapbox/light-v9',
                zoom: 11.2,
                center: [106.800,-6.59988]
            });
            // Add zoom and rotation controls to the map.
            map.addControl(new mapboxgl.NavigationControl(),'top-left');

			map.on('load',function() {

				//Bogor Barat
                  map.addLayer({
                      'id': 'Bogor Barat1',
                      'type': 'fill',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polygon/PBogorBarat.geojson'
                      },
                      'paint': {
                          'fill-color': '#ffb2e2',
                          'fill-opacity': 0.5
                      }
                  });
                  //Bogor Selatan
                  map.addLayer({
                      'id': 'Bogor Selatan1',
                      'type': 'fill',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polygon/PBogorSelatan.geojson'
                      },
                      'paint': {
                          'fill-color': '#c0ffb2',
                          'fill-opacity': 0.5
                      }
                  });
                  //Bogor Tengah
                  map.addLayer({
                      'id': 'Bogor Tengah1',
                      'type': 'fill',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polygon/PBogorTengah.geojson'
                      },
                      'paint': {
                          'fill-color': '#ff8468',
                          'fill-opacity': 0.5
                      }
                  });
                  //Bogor Timur
                  map.addLayer({
                      'id': 'Bogor Timur1',
                      'type': 'fill',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polygon/PBogorTimur.geojson'
                      },
                      'paint': {
                          'fill-color': '#f4ffb2',
                          'fill-opacity': 0.5
                      }
                  });
                  //Bogor Utara
                  map.addLayer({
                      'id': 'Bogor Utara1',
                      'type': 'fill',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polygon/PBogorUtara.geojson'
                      },
                      'paint': {
                          'fill-color': '#b2d3ff',
                          'fill-opacity': 0.5
                      }
                  });
                  //Tanah Sareal
                  map.addLayer({
                      'id': 'Tanah Sareal1',
                      'type': 'fill',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polygon/PTanahSareal.geojson'
                      },
                      'paint': {
                          'fill-color': '#92f98e',
                          'fill-opacity': 0.5
                      }
                  });


				  //Jaringan
				  map.addLayer({
                      'id': 'ciluar',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/ciluar.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'cimahpar1',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/cimahpar1.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'ciparigi',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/ciparigi.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'katulampa',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/katulampa.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'kalurahan margajaya',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/kelurahan_margajaya.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'kelurahan margajaya2',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/kelurahan_margajaya2.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'kelurahan mulyaharja',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/kelurahan_mulyaharja.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'kelurahan rancamaya',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/kelurahan_rancamaya.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'kelurahan sukaresmi',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/kelurahan_sukaresmi.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  map.addLayer({
                      'id': 'kelurahan sukaresmi2',
                      'type': 'line',
                      'source': {
                          'type': 'geojson',
                          'data': 'http://localhost/sumber_air/assets/polyline/kelurahan_sukaresmi2.geojson'
                      },
                      'paint': {
                          'line-color': '#229912'
                      }
                  });

				  //marker
					  map.addSource('marker_b_selatan',{
					  type: 'geojson',
					  data: {
						"type": "FeatureCollection",
						"features": [
						  <?php
						  $numItem = $marker_b_selatan->num_rows();
						  $i = 0;
						  foreach ($marker_b_selatan->result_array() as $row => $value) {
							?>
							{
							  "type": "Feature",
							  "geometry": {
								"type": "Point",
								"coordinates": [
								  <?php echo (float)$value['latitude']; ?>,
								  <?php echo (float)$value['longitude']; ?>
								]
							  },
							  "properties": {
									"alamat": "Alamat : <?php echo $value['almt']; ?>, RT: <?php echo $value['rt']; ?>, RW: <?php echo $value['rw']; ?><br>",
	                "region": "Kecamatan : <?php echo $value['nm_kecamatan']; ?>, Kelurahan : <?php echo $value['nm_desa']; ?><br>",
	                "lahan" : "Luas Lahan : <?php echo $value['ls_lahan']; ?>, Terbangun : <?php echo $value['ls_lahan_terbangun']; ?>, Perkiraan sis lahan: <?php echo $value['perkiraan_sisa_lhn']; ?>, Status lahan : <?php echo $value['status_lahan']; ?><br>",
	                "pendanaan" : "Pendanaan : <?php echo $value['pendanaan']; ?><br>",
	                "tahun_pembangunan" : "Tahun Pembangunan : <?php echo $value['thn_pembangunan']; ?><br>",
	                "sumber_air" : "Sumber Air : <?php echo $value['sumber_air']; ?>, Arah Mata Air : <?php echo $value['arah_mata_air']; ?>, Kedalaman : <?php echo $value['kedalaman']; ?><br>",
	                "debit" : "Debit Liter : <?php echo $value['debit_liter']; ?>, Debit detik : <?php echo $value['debit_detik']; ?>, Debit Air : <?php echo $value['debit_air']; ?>, Panjang Pipa : <?php echo $value['pjg_pipa']; ?><br>",
	                "kondisi" : "Kebersihan : <?php echo $value['kebersihan']; ?>, Fungsi : <?php echo $value['kondisi']; ?>, Kondisi : <?php echo $value['kondisi']; ?>, Pemeliharaan : <?php echo $value['pemeliharaan']; ?><br>",
	                "jar_pipa" : "Jml. SR Terpasang : <?php echo $value['jml_sr_terpasang']; ?>, Kondisi Kelancaran : <?php echo $value['kondisi_kelancaran_sis_pengalir']; ?>, Kondisi Perawatan : <?php echo $value['kondisi_perawatan_sis_pengalir']; ?>, Kondisi Fungsi : <?php echo $value['kondisi_fungsi_sis_pengalir']; ?>, Hidran : <?php echo $value['ketersediaan_hidran']; ?>, Meteran : <?php echo $value['meteran']; ?><br>",
	                "sis_pengolahan" : "Sistem Pengolahan : <?php echo $value['sis_pengolahan']; ?>, Bangunan Pelengkap : <?php echo $value['bangunan_pelengkap']; ?>, Panjang : <?php echo $value['panjang']; ?>, Lebar : <?php echo $value['lebar']; ?>, Tinggi : <?php echo $value['tinggi']; ?><br>",
	                "foto" : "<img class='img img-thumbnail' style='width:300px;height100px;' src='<?php echo base_url("uploads/".$value['foto']); ?>'>",
	                "icon":"marker"
							  }
							}<?php
							  if(++$i === $numItem){
								echo "";
							  } else {
								echo ",";
							  }
							?>
							<?php
						  }
						  ?>
						]
					  }
					});
					map.addLayer({
					  'id': 'Bogor Selatan',
					  'source': 'marker_b_selatan',
					  'type': 'circle',
					  'layout' : {
						  'visibility': 'visible'
					  },
					  'paint': {
						'circle-radius': 5,
						'circle-color': '#267016'
					  }
					});

					//marker
					  map.addSource('marker_b_barat',{
					  type: 'geojson',
					  data: {
						"type": "FeatureCollection",
						"features": [
						  <?php
						  $numItem = $marker_b_barat->num_rows();
						  $i = 0;
						  foreach ($marker_b_barat->result_array() as $row => $value) {
							?>
							{
							  "type": "Feature",
							  "geometry": {
								"type": "Point",
								"coordinates": [
								  <?php echo (float)$value['latitude']; ?>,
								  <?php echo (float)$value['longitude']; ?>
								]
							  },
							  "properties": {
									"alamat": "Alamat : <?php echo $value['almt']; ?>, RT: <?php echo $value['rt']; ?>, RW: <?php echo $value['rw']; ?><br>",
	                "region": "Kecamatan : <?php echo $value['nm_kecamatan']; ?>, Kelurahan : <?php echo $value['nm_desa']; ?><br>",
	                "lahan" : "Luas Lahan : <?php echo $value['ls_lahan']; ?>, Terbangun : <?php echo $value['ls_lahan_terbangun']; ?>, Perkiraan sis lahan: <?php echo $value['perkiraan_sisa_lhn']; ?>, Status lahan : <?php echo $value['status_lahan']; ?><br>",
	                "pendanaan" : "Pendanaan : <?php echo $value['pendanaan']; ?><br>",
	                "tahun_pembangunan" : "Tahun Pembangunan : <?php echo $value['thn_pembangunan']; ?><br>",
	                "sumber_air" : "Sumber Air : <?php echo $value['sumber_air']; ?>, Arah Mata Air : <?php echo $value['arah_mata_air']; ?>, Kedalaman : <?php echo $value['kedalaman']; ?><br>",
	                "debit" : "Debit Liter : <?php echo $value['debit_liter']; ?>, Debit detik : <?php echo $value['debit_detik']; ?>, Debit Air : <?php echo $value['debit_air']; ?>, Panjang Pipa : <?php echo $value['pjg_pipa']; ?><br>",
	                "kondisi" : "Kebersihan : <?php echo $value['kebersihan']; ?>, Fungsi : <?php echo $value['kondisi']; ?>, Kondisi : <?php echo $value['kondisi']; ?>, Pemeliharaan : <?php echo $value['pemeliharaan']; ?><br>",
	                "jar_pipa" : "Jml. SR Terpasang : <?php echo $value['jml_sr_terpasang']; ?>, Kondisi Kelancaran : <?php echo $value['kondisi_kelancaran_sis_pengalir']; ?>, Kondisi Perawatan : <?php echo $value['kondisi_perawatan_sis_pengalir']; ?>, Kondisi Fungsi : <?php echo $value['kondisi_fungsi_sis_pengalir']; ?>, Hidran : <?php echo $value['ketersediaan_hidran']; ?>, Meteran : <?php echo $value['meteran']; ?><br>",
	                "sis_pengolahan" : "Sistem Pengolahan : <?php echo $value['sis_pengolahan']; ?>, Bangunan Pelengkap : <?php echo $value['bangunan_pelengkap']; ?>, Panjang : <?php echo $value['panjang']; ?>, Lebar : <?php echo $value['lebar']; ?>, Tinggi : <?php echo $value['tinggi']; ?><br>",
	                "foto" : "<img class='img img-thumbnail' style='width:300px;height100px;' src='<?php echo base_url("uploads/".$value['foto']); ?>'>",
	                "icon":"marker"
							  }
							}<?php
							  if(++$i === $numItem){
								echo "";
							  } else {
								echo ",";
							  }
							?>
							<?php
						  }
						  ?>
						]
					  }
					});
					map.addLayer({
					  'id': 'Bogor Barat',
					  'source': 'marker_b_barat',
					  'type': 'circle',
					  'layout' : {
						  'visibility': 'visible'
					  },
					  'paint': {
						'circle-radius': 5,
						'circle-color': '#ff28ae'
					  }
					});

					map.addSource('marker_b_timur',{
					  type: 'geojson',
					  data: {
						"type": "FeatureCollection",
						"features": [
						  <?php
						  $numItem = $marker_b_timur->num_rows();
						  $i = 0;
						  foreach ($marker_b_timur->result_array() as $row => $value) {
							?>
							{
							  "type": "Feature",
							  "geometry": {
								"type": "Point",
								"coordinates": [
								  <?php echo (float)$value['latitude']; ?>,
								  <?php echo (float)$value['longitude']; ?>
								]
							  },
							  "properties": {
									"alamat": "Alamat : <?php echo $value['almt']; ?>, RT: <?php echo $value['rt']; ?>, RW: <?php echo $value['rw']; ?><br>",
	                "region": "Kecamatan : <?php echo $value['nm_kecamatan']; ?>, Kelurahan : <?php echo $value['nm_desa']; ?><br>",
	                "lahan" : "Luas Lahan : <?php echo $value['ls_lahan']; ?>, Terbangun : <?php echo $value['ls_lahan_terbangun']; ?>, Perkiraan sis lahan: <?php echo $value['perkiraan_sisa_lhn']; ?>, Status lahan : <?php echo $value['status_lahan']; ?><br>",
	                "pendanaan" : "Pendanaan : <?php echo $value['pendanaan']; ?><br>",
	                "tahun_pembangunan" : "Tahun Pembangunan : <?php echo $value['thn_pembangunan']; ?><br>",
	                "sumber_air" : "Sumber Air : <?php echo $value['sumber_air']; ?>, Arah Mata Air : <?php echo $value['arah_mata_air']; ?>, Kedalaman : <?php echo $value['kedalaman']; ?><br>",
	                "debit" : "Debit Liter : <?php echo $value['debit_liter']; ?>, Debit detik : <?php echo $value['debit_detik']; ?>, Debit Air : <?php echo $value['debit_air']; ?>, Panjang Pipa : <?php echo $value['pjg_pipa']; ?><br>",
	                "kondisi" : "Kebersihan : <?php echo $value['kebersihan']; ?>, Fungsi : <?php echo $value['kondisi']; ?>, Kondisi : <?php echo $value['kondisi']; ?>, Pemeliharaan : <?php echo $value['pemeliharaan']; ?><br>",
	                "jar_pipa" : "Jml. SR Terpasang : <?php echo $value['jml_sr_terpasang']; ?>, Kondisi Kelancaran : <?php echo $value['kondisi_kelancaran_sis_pengalir']; ?>, Kondisi Perawatan : <?php echo $value['kondisi_perawatan_sis_pengalir']; ?>, Kondisi Fungsi : <?php echo $value['kondisi_fungsi_sis_pengalir']; ?>, Hidran : <?php echo $value['ketersediaan_hidran']; ?>, Meteran : <?php echo $value['meteran']; ?><br>",
	                "sis_pengolahan" : "Sistem Pengolahan : <?php echo $value['sis_pengolahan']; ?>, Bangunan Pelengkap : <?php echo $value['bangunan_pelengkap']; ?>, Panjang : <?php echo $value['panjang']; ?>, Lebar : <?php echo $value['lebar']; ?>, Tinggi : <?php echo $value['tinggi']; ?><br>",
	                "foto" : "<img class='img img-thumbnail' style='width:300px;height100px;' src='<?php echo base_url("uploads/".$value['foto']); ?>'>",
	                "icon":"marker"
							  }
							}<?php
							  if(++$i === $numItem){
								echo "";
							  } else {
								echo ",";
							  }
							?>
							<?php
						  }
						  ?>
						]
					  }
					});
					map.addLayer({
					  'id': 'Bogor Timur',
					  'source': 'marker_b_timur',
					  'type': 'circle',
					  'layout' : {
						  'visibility': 'visible'
					  },
					  'paint': {
						'circle-radius': 5,
						'circle-color': '#dfff21'
					  }
					});

					map.addSource('marker_b_utara',{
					  type: 'geojson',
					  data: {
						"type": "FeatureCollection",
						"features": [
						  <?php
						  $numItem = $marker_b_utara->num_rows();
						  $i = 0;
						  foreach ($marker_b_utara->result_array() as $row => $value) {
							?>
							{
							  "type": "Feature",
							  "geometry": {
								"type": "Point",
								"coordinates": [
								  <?php echo (float)$value['latitude']; ?>,
								  <?php echo (float)$value['longitude']; ?>
								]
							  },
							  "properties": {
									"alamat": "Alamat : <?php echo $value['almt']; ?>, RT: <?php echo $value['rt']; ?>, RW: <?php echo $value['rw']; ?><br>",
	                "region": "Kecamatan : <?php echo $value['nm_kecamatan']; ?>, Kelurahan : <?php echo $value['nm_desa']; ?><br>",
	                "lahan" : "Luas Lahan : <?php echo $value['ls_lahan']; ?>, Terbangun : <?php echo $value['ls_lahan_terbangun']; ?>, Perkiraan sis lahan: <?php echo $value['perkiraan_sisa_lhn']; ?>, Status lahan : <?php echo $value['status_lahan']; ?><br>",
	                "pendanaan" : "Pendanaan : <?php echo $value['pendanaan']; ?><br>",
	                "tahun_pembangunan" : "Tahun Pembangunan : <?php echo $value['thn_pembangunan']; ?><br>",
	                "sumber_air" : "Sumber Air : <?php echo $value['sumber_air']; ?>, Arah Mata Air : <?php echo $value['arah_mata_air']; ?>, Kedalaman : <?php echo $value['kedalaman']; ?><br>",
	                "debit" : "Debit Liter : <?php echo $value['debit_liter']; ?>, Debit detik : <?php echo $value['debit_detik']; ?>, Debit Air : <?php echo $value['debit_air']; ?>, Panjang Pipa : <?php echo $value['pjg_pipa']; ?><br>",
	                "kondisi" : "Kebersihan : <?php echo $value['kebersihan']; ?>, Fungsi : <?php echo $value['kondisi']; ?>, Kondisi : <?php echo $value['kondisi']; ?>, Pemeliharaan : <?php echo $value['pemeliharaan']; ?><br>",
	                "jar_pipa" : "Jml. SR Terpasang : <?php echo $value['jml_sr_terpasang']; ?>, Kondisi Kelancaran : <?php echo $value['kondisi_kelancaran_sis_pengalir']; ?>, Kondisi Perawatan : <?php echo $value['kondisi_perawatan_sis_pengalir']; ?>, Kondisi Fungsi : <?php echo $value['kondisi_fungsi_sis_pengalir']; ?>, Hidran : <?php echo $value['ketersediaan_hidran']; ?>, Meteran : <?php echo $value['meteran']; ?><br>",
	                "sis_pengolahan" : "Sistem Pengolahan : <?php echo $value['sis_pengolahan']; ?>, Bangunan Pelengkap : <?php echo $value['bangunan_pelengkap']; ?>, Panjang : <?php echo $value['panjang']; ?>, Lebar : <?php echo $value['lebar']; ?>, Tinggi : <?php echo $value['tinggi']; ?><br>",
	                "foto" : "<img class='img img-thumbnail' style='width:300px;height100px;' src='<?php echo base_url("uploads/".$value['foto']); ?>'>",
	                "icon":"marker"
							  }
							}<?php
							  if(++$i === $numItem){
								echo "";
							  } else {
								echo ",";
							  }
							?>
							<?php
						  }
						  ?>
						]
					  }
					});
					map.addLayer({
					  'id': 'Bogor Utara',
					  'source': 'marker_b_utara',
					  'type': 'circle',
					  'layout' : {
						  'visibility': 'visible'
					  },
					  'paint': {
						'circle-radius': 5,
						'circle-color': '#328aff'
					  }
					});

					map.addSource('marker_b_tengah',{
					  type: 'geojson',
					  data: {
						"type": "FeatureCollection",
						"features": [
						  <?php
						  $numItem = $marker_b_tengah->num_rows();
						  $i = 0;
						  foreach ($marker_b_tengah->result_array() as $row => $value) {
							?>
							{
							  "type": "Feature",
							  "geometry": {
								"type": "Point",
								"coordinates": [
								  <?php echo (float)$value['latitude']; ?>,
								  <?php echo (float)$value['longitude']; ?>
								]
							  },
							  "properties": {
									"alamat": "Alamat : <?php echo $value['almt']; ?>, RT: <?php echo $value['rt']; ?>, RW: <?php echo $value['rw']; ?><br>",
	                "region": "Kecamatan : <?php echo $value['nm_kecamatan']; ?>, Kelurahan : <?php echo $value['nm_desa']; ?><br>",
	                "lahan" : "Luas Lahan : <?php echo $value['ls_lahan']; ?>, Terbangun : <?php echo $value['ls_lahan_terbangun']; ?>, Perkiraan sis lahan: <?php echo $value['perkiraan_sisa_lhn']; ?>, Status lahan : <?php echo $value['status_lahan']; ?><br>",
	                "pendanaan" : "Pendanaan : <?php echo $value['pendanaan']; ?><br>",
	                "tahun_pembangunan" : "Tahun Pembangunan : <?php echo $value['thn_pembangunan']; ?><br>",
	                "sumber_air" : "Sumber Air : <?php echo $value['sumber_air']; ?>, Arah Mata Air : <?php echo $value['arah_mata_air']; ?>, Kedalaman : <?php echo $value['kedalaman']; ?><br>",
	                "debit" : "Debit Liter : <?php echo $value['debit_liter']; ?>, Debit detik : <?php echo $value['debit_detik']; ?>, Debit Air : <?php echo $value['debit_air']; ?>, Panjang Pipa : <?php echo $value['pjg_pipa']; ?><br>",
	                "kondisi" : "Kebersihan : <?php echo $value['kebersihan']; ?>, Fungsi : <?php echo $value['kondisi']; ?>, Kondisi : <?php echo $value['kondisi']; ?>, Pemeliharaan : <?php echo $value['pemeliharaan']; ?><br>",
	                "jar_pipa" : "Jml. SR Terpasang : <?php echo $value['jml_sr_terpasang']; ?>, Kondisi Kelancaran : <?php echo $value['kondisi_kelancaran_sis_pengalir']; ?>, Kondisi Perawatan : <?php echo $value['kondisi_perawatan_sis_pengalir']; ?>, Kondisi Fungsi : <?php echo $value['kondisi_fungsi_sis_pengalir']; ?>, Hidran : <?php echo $value['ketersediaan_hidran']; ?>, Meteran : <?php echo $value['meteran']; ?><br>",
	                "sis_pengolahan" : "Sistem Pengolahan : <?php echo $value['sis_pengolahan']; ?>, Bangunan Pelengkap : <?php echo $value['bangunan_pelengkap']; ?>, Panjang : <?php echo $value['panjang']; ?>, Lebar : <?php echo $value['lebar']; ?>, Tinggi : <?php echo $value['tinggi']; ?><br>",
	                "foto" : "<img class='img img-thumbnail' style='width:300px;height100px;' src='<?php echo base_url("uploads/".$value['foto']); ?>'>",
	                "icon":"marker"
							  }
							}<?php
							  if(++$i === $numItem){
								echo "";
							  } else {
								echo ",";
							  }
							?>
							<?php
						  }
						  ?>
						]
					  }
					});
					map.addLayer({
					  'id': 'Bogor Tengah',
					  'source': 'marker_b_tengah',
					  'type': 'circle',
					  'layout' : {
						  'visibility': 'visible'
					  },
					  'paint': {
						'circle-radius': 5,
						'circle-color': '#ff4e26'
					  }
					});

					map.addSource('marker_tan_sareal',{
					  type: 'geojson',
					  data: {
						"type": "FeatureCollection",
						"features": [
						  <?php
						  $numItem = $marker_tan_sareal->num_rows();
						  $i = 0;
						  foreach ($marker_tan_sareal->result_array() as $row => $value) {
							?>
							{
							  "type": "Feature",
							  "geometry": {
								"type": "Point",
								"coordinates": [
								  <?php echo (float)$value['latitude']; ?>,
								  <?php echo (float)$value['longitude']; ?>
								]
							  },
							  "properties": {
									"alamat": "Alamat : <?php echo $value['almt']; ?>, RT: <?php echo $value['rt']; ?>, RW: <?php echo $value['rw']; ?><br>",
	                "region": "Kecamatan : <?php echo $value['nm_kecamatan']; ?>, Kelurahan : <?php echo $value['nm_desa']; ?><br>",
	                "lahan" : "Luas Lahan : <?php echo $value['ls_lahan']; ?>, Terbangun : <?php echo $value['ls_lahan_terbangun']; ?>, Perkiraan sis lahan: <?php echo $value['perkiraan_sisa_lhn']; ?>, Status lahan : <?php echo $value['status_lahan']; ?><br>",
	                "pendanaan" : "Pendanaan : <?php echo $value['pendanaan']; ?><br>",
	                "tahun_pembangunan" : "Tahun Pembangunan : <?php echo $value['thn_pembangunan']; ?><br>",
	                "sumber_air" : "Sumber Air : <?php echo $value['sumber_air']; ?>, Arah Mata Air : <?php echo $value['arah_mata_air']; ?>, Kedalaman : <?php echo $value['kedalaman']; ?><br>",
	                "debit" : "Debit Liter : <?php echo $value['debit_liter']; ?>, Debit detik : <?php echo $value['debit_detik']; ?>, Debit Air : <?php echo $value['debit_air']; ?>, Panjang Pipa : <?php echo $value['pjg_pipa']; ?><br>",
	                "kondisi" : "Kebersihan : <?php echo $value['kebersihan']; ?>, Fungsi : <?php echo $value['kondisi']; ?>, Kondisi : <?php echo $value['kondisi']; ?>, Pemeliharaan : <?php echo $value['pemeliharaan']; ?><br>",
	                "jar_pipa" : "Jml. SR Terpasang : <?php echo $value['jml_sr_terpasang']; ?>, Kondisi Kelancaran : <?php echo $value['kondisi_kelancaran_sis_pengalir']; ?>, Kondisi Perawatan : <?php echo $value['kondisi_perawatan_sis_pengalir']; ?>, Kondisi Fungsi : <?php echo $value['kondisi_fungsi_sis_pengalir']; ?>, Hidran : <?php echo $value['ketersediaan_hidran']; ?>, Meteran : <?php echo $value['meteran']; ?><br>",
	                "sis_pengolahan" : "Sistem Pengolahan : <?php echo $value['sis_pengolahan']; ?>, Bangunan Pelengkap : <?php echo $value['bangunan_pelengkap']; ?>, Panjang : <?php echo $value['panjang']; ?>, Lebar : <?php echo $value['lebar']; ?>, Tinggi : <?php echo $value['tinggi']; ?><br>",
	                "foto" : "<img class='img img-thumbnail' style='width:300px;height100px;' src='<?php echo base_url("uploads/".$value['foto']); ?>'>",
	                "icon":"marker"
							  }
							}<?php
							  if(++$i === $numItem){
								echo "";
							  } else {
								echo ",";
							  }
							?>
							<?php
						  }
						  ?>
						]
					  }
					});
					map.addLayer({
					  'id': 'Tanah Sareal',
					  'source': 'marker_tan_sareal',
					  'type': 'circle',
					  'layout' : {
						  'visibility': 'visible'
					  },
					  'paint': {
						'circle-radius': 5,
						'circle-color': '#25c41f'
					  }
					});
			});

			var toggleableLayerIds = [ 'Bogor Selatan', 'Bogor Timur', 'Bogor Utara', 'Bogor Tengah' ,'Bogor Barat', 'Tanah Sareal'];
				var clr = ['#c0ffb2','#f4ffb2', '#b2d3ff', '#ff8468', '#ffb2e2', '#92f98e'];
				for (var i = 0; i < toggleableLayerIds.length; i++) {
					var id = toggleableLayerIds[i];
					var color = clr[i];
					var link = document.createElement('a');
					link.href = '#';
					link.className = 'active';
					link.textContent = id;

					var legend = document.createElement('div');
					legend.className = 'legend';
					legend.style.backgroundColor = color;
					legend.style.height = '10px';
					legend.style.width = '10px';


					link.onclick = function (e) {
						var clickedLayer = this.textContent;
						e.preventDefault();
						e.stopPropagation();

						var visibility = map.getLayoutProperty(clickedLayer, 'visibility');

						if (visibility === 'visible') {
							map.setLayoutProperty(clickedLayer, 'visibility', 'none');
							this.className = '';
						} else {
							this.className = 'active';
							map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
						}
					};

					var layers = document.getElementById('menu');
					layers.appendChild(link);
					link.appendChild(legend);
				}

				//popup
				map.on('click', function (e) {
					var features = map.queryRenderedFeatures(e.point, { layers: [ 'Tanah Sareal', 'Bogor Tengah', 'Bogor Timur', 'Bogor Barat','Bogor Selatan', 'Bogor Utara' ] });

					if (!features.length) {
						return;
					}

					var feature = features[0];

					// Populate the popup and set its coordinates
					// based on the feature found.
					var popup = new mapboxgl.Popup()
						.setLngLat(feature.geometry.coordinates)
						.setHTML('<div class="col-md-6">'+feature.properties.foto+'</div><div class="col-md-6" style="color: black;">'+
	          feature.properties.alamat+
	          feature.properties.region+
	          feature.properties.lahan+
	          feature.properties.tahun_pembangunan+
	          feature.properties.pendanaan+
	          feature.properties.tahun_pembangunan+
	          feature.properties.sumber_air+
	          feature.properties.debit+
	          feature.properties.kondisi+
	          feature.properties.jar_pipa+
	          feature.properties.sis_pengolahan+
	          '</div>')
						.addTo(map);
					});
		</script>

<?php include 'template/footer.php'; ?>
