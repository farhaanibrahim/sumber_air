<?php foreach($user->result() as $row){} ?>
<?php include 'template/header.php' ?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="header">
                        <h4 class="title">Title</h4>
                    </div>
					<div class="content">
						<div class="box box-info">
							<div class="box-header with-border">
							  <h3 class="box-title">Edit Password</h3>
							</div>
							
							<div class="box-body">
							
								<div class="form-group">
									<label><b>Username</b> : <?php echo $row->username; ?></label>
								</div>
								
								<div class="form-group">
									<label>Old Password</label>
									<input type="password" name="old_password" class="form-control" placeholder="Password">
								</div>
								
								<div class="form-group">
									<label>New Password</label>
									<input type="password" name="new_password" class="form-control" placeholder="Password">
								</div>
								
								<div class="form-group">
									<label>Re-type Password</label>
									<input type="password" name="password2" class="form-control" placeholder="Re-type Password">
								</div>
								
								<div class="form-group">
									<label></label>
									<input type="submit" name="btnSubmit" class="btn btn-primary" value="Save">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'template/footer.php' ?>