<?php include 'template/header.php' ?>
<style>


</style>
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="header">
                        <h4 class="title">Input Data Sumber Air</h4>
                    </div>
					<div class="content">
						<ul class="nav nav-tabs" role="tablist">
						  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">  <span>Lokasi</span></a></li>
						  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">  <span>Sistem Air Minum</span></a></li>
						  <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">  <span>Jaringan Pipa</span></a></li>
						  <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">  <span>Sistem Pengolahan</span></a></li>
						  <li role="presentation"><a href="#extra" aria-controls="settings" role="tab" data-toggle="tab">  <span>Ekonomi</span></a></li>
						  <li role="presentation"><a href="#gbr_umum" aria-controls="settings" role="tab" data-toggle="tab">  <span>Gbr. Daerah Pelayanan</span></a></li>
						  <li role="presentation"><a href="#potensi" aria-controls="settings" role="tab" data-toggle="tab">  <span>Potensi</span></a></li>
						</ul>

						<!-- Tab panes -->
						<?php if ($this->session->flashdata('error')): ?>
							<div class="alert alert-danger">
								<strong>Failed!</strong><?php echo $this->session->flashdata('error'); ?>
							</div>
						<?php endif; ?>
						<form action="<?php echo base_url('admin/proses_input_data'); ?>" method="post" enctype="multipart/form-data">
						<div class="tab-content">
						  <div role="tabpanel" class="tab-pane active" id="home">
							<div class="row">
								<!--<div class="col-md-12"><h4>Input Lokasi</h4></div>-->
								<div class="col-md-4">
									<div class="form-group">
										<label>Kecamatan</label>
										<select class="form-control" name="kecamatan" id="kecamatan">
											<option>Kecamatan</option>
											<?php
											foreach($kecamatan->result() as $kecamatan)
											{
												?>
												<option value="<?php echo $kecamatan->id_kecamatan; ?>"><?php echo $kecamatan->nm_kecamatan; ?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group">
										<label>Kelurahan</label>
										<select class="form-control" name="kelurahan" id="kelurahan"disabled>
											<option>Kelurahan</option>
										</select>
									</div>
									<div class="form-group">
										<label>RW</label>
										<input type="text" name="rw" class="form-control" placeholder="RW">
									</div>
									<div class="form-group">
										<label>RT</label>
										<input type="text" name="rt" class="form-control" placeholder="RT">
									</div>
									<div class="form-group">
										<label>Alamat</label>
										<textarea class="form-control" name="almt" placeholder="Alamat"></textarea>
									</div>

								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>No. SK legalitas Hibah</label>
										<input type="text" name="no_sk_lh" class="form-control" placeholder="No. SK legalitas Hibah">
									</div>
									<div class="form-group">
										<label>No. SK Kelurahan</label>
										<input type="text" name="no_sk_kel" class="form-control" placeholder="No. SK legalitas Hibah">
									</div>
									<div class="form-group">
										<label>Luas Lahan</label>
										<input type="text" name="ls_lahan" class="form-control" placeholder="Luas Lahan">
									</div>
									<div class="form-group">
										<label>Luas Lahan terbangun</label>
										<input type="text" name="ls_lahan_terbangun" class="form-control" placeholder="Luas Lahan Terbangun">
									</div>
									<div class="form-group">
										<label>Perkiraan Sisa Lahan</label>
										<input type="text" name="perkiraan_sisa_lhn" class="form-control" placeholder="Perkiraan Sisa Lahan">
									</div>

								</div>
								<div class="col-md-4">

									<div class="form-group">
										<label>Status Lahan</label>
										<input type="text" name="status_lahan" class="form-control" placeholder="Status Lahan">
									</div>
									<div class="form-group">
										<label>Pendanaan</label>
										<input type="text" name="pendanaan" class="form-control" placeholder="Pendanaan">
									</div>
									<div class="form-group">
										<label>Tahun Pembangunan</label>
										<input type="text" name="thn_pembangunan" class="form-control" placeholder="Tahun Pembangunan">
									</div>
									<div class="form-group">
										<label>Longitude</label>
										<input type="text" name="longitude" class="form-control" placeholder="Longitude">
									</div>
									<div class="form-group">
										<label>Latitude</label>
										<input type="text" name="latitude" class="form-control" placeholder="Latitude">
									</div>
									<div class="form-group">
										<label>Foto</label>
										<input type="file" name="foto">
									</div>
									<div class="form-group" align="right">
										<br>
										<a href="#profile" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="profile">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Sumber Air</label>
										<select name="sumber_air" class="form-control">
											<option> Sumber Air</option>
											<option value="Mata Air"> Mata Air</option>
											<option value="Air Tanah"> Air Tanah</option>
										</select>
									</div>
									<div class="form-group">
										<label>Arah Mata Air</label>
										<input type="text" name="arah_mata_air" class="form-control" placeholder="Arah Mata Air">
									</div>
									<div class="form-group">
										<label>Kedalaman</label>
										<input type="text" name="kedalaman" class="form-control" placeholder="Kedalaman">
									</div>
									<div class="form-group">
										<label>Debit Liter</label>
										<input type="text" name="debit_liter" class="form-control" placeholder="Debit Liter">
									</div>
									<div class="form-group">
										<label>Debit Detik</label>
										<input type="text" name="debit_detik" class="form-control" placeholder="Debit Detik">
									</div>
									<div class="form-group">
										<label>Debit Air</label>
										<input type="text" name="debit_air" class="form-control" placeholder="Debit Air">
									</div>

								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Panjang Pipa</label>
										<input type="text" name="pjg_pipa" class="form-control" placeholder="Panjang Pipa">
									</div>
									<div class="form-group">
										<label>Kebersihan</label>
										<select name="kebersihan" class="form-control">
											<option>Kebersihan</option>
											<option value="terawat">Terawat</option>
											<option value="kurang terawat">Kurang Terawat</option>
											<option value="tidak terawat">Tidak Terawat</option>
										</select>
									</div>
									<div class="form-group">
										<label>Fungsi</label>
										<select name="fungsi" class="form-control">
											<option>Fungsi</option>
											<option value="berfungsi">berfungsi</option>
											<option value="tidak berfungsi">tidak berfungsi</option>
										</select>
									</div>
									<div class="form-group">
										<label>Kondisi</label>
										<select name="kondisi" class="form-control">
											<option>Kondisi</option>
											<option value="baik">baik</option>
											<option value="sebagian rusak">sebagian rusak</option>
											<option value="rusak">rusak</option>
										</select>
									</div>
									<div class="form-group">
										<label>Pemeliharaan</label>
										<select name="pemeliharaan" class="form-control">
											<option>Kebersihan</option>
											<option value="ada dan rutin">ada dan rutin</option>
											<option value="ada dan tidak rutin">ada dan tidak rutin</option>
											<option value="tidak ada">tidak ada</option>
										</select>
									</div>
									<div class="form-group">
										<label>Penjelasan</label>
										<textarea name="penjelasan_sam" class="form-control" placeholder="Penjelasan"></textarea>
									</div>
									<div class="form-group" align="right">
										<br>
										<a href="#messages" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="messages">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
								<label>Jumlah SR Terpasang</label>
								<input type="text" name="jml_sr_terpasang" class="form-control" placeholder="Jumlah SR Terpasang">
							</div>
							<div class="form-group">
								<label>Kondisi Kelancaran Sistem Pengalir</label>
								<select name="kondisi_kelancaran_sis_pengalir" class="form-control">
									<option>Kondisi</option>
									<option value="lancar">lancar</option>
									<option value="tidak lancar">tidak lancar</option>
								</select>
							</div>
							<div class="form-group">
								<label>Kondisi Perawatan Sistem Pengalir</label>
								<select name="kondisi_perawatan_sis_pengalir" class="form-control">
									<option>Kondisi</option>
									<option value="terawat">terawat</option>
									<option value="tidak terawat">tidak terawat</option>
								</select>
							</div>
							<div class="form-group">
								<label>Kondisi Fungsi Sistem Pengalir</label>
								<select name="kondisi_fungsi_sis_pengalir" class="form-control">
									<option>Kondisi</option>
									<option value="optimal">optimal</option>
									<option value="belum optimal">belum optimal</option>
								</select>
							</div>
							<div class="form-group">
								<label>Ketersediaan Hidran</label>
								<select name="ketersediaan_hidran" class="form-control">
									<option>Ketersediaan Hidran</option>
									<option value="ada">ada</option>
									<option value="tidak ada">tidak ada</option>
								</select>

								<div class="form-group">
										<label>Jumlah Hidran</label>
										<input type="text" name="jml_hidran" class="form-control" placeholder="Jumlah Hidran">
									</div>
							</div>
								</div>
								<div class="col-md-6">

									<div class="form-group">
										<label>Meteran</label>
										<select name="meteran" class="form-control">
											<option></option>
											<option value="ada">ada</option>
											<option value="tidak ada">tidak ada</option>
										</select>
									</div>
									<div class="form-group">
										<label>Jumlah Meteran</label>
										<input type="text" name="jml_meteran" class="form-control" placeholder="Jumlah Meteran">
									</div>
									<div class="form-group">
										<label>Pemeliharaan Sistem Jaringan</label>
										<select name="pemeliharaan_sis_jaringan" class="form-control">
											<option></option>
											<option value="ada dan rutin">ada dan rutin</option>
											<option value="ada dan tidak rutin">ada dan tidak rutin</option>
											<option value="tidak ada">tidak ada</option>
										</select>
									</div>
									<div class="form-group">
										<label>Penjelasan</label>
										<textarea name="penjelasan_jar_pipa" class="form-control" placeholder="Penjelasan"></textarea>
									</div>
									<div class="form-group">
										<label>Lebar Jalan</label>
										<textarea name="lebar_jln" class="form-control" placeholder="Lebar Jalan"></textarea>
									</div>
									<div class="form-group" align="right">
										<br>
										<a href="#settings" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="settings">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Sistem Pengolahan</label>
										<select name="sis_pengolahan" class="form-control">
											<option></option>
											<option value="ada">ada</option>
											<option value="tidak ada">tidak ada</option>
										</select>
									</div>
									<div class="form-group">
										<label>Bangunan Pelengkap</label>
										<textarea name="bangunan_pelengkap" class="form-control" placeholder="Bangunan Pelengkap"></textarea>
									</div>
									<div class="form-group">
										<label>Panjang</label>
										<input type="text" name="panjang" class="form-control" placeholder="Panjang">
									</div>
									<div class="form-group">
										<label>Lebar</label>
										<input type="text" name="lebar" class="form-control" placeholder="Lebar">
									</div>
									<div class="form-group">
										<label>Tinggi</label>
										<input type="text" name="tinggi" class="form-control" placeholder="Tinggi">
									</div>
									<div class="form-group">
										<label>Jari-jari</label>
										<input type="text" name="jari-jari" class="form-control" placeholder="Jari-jari">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Kapasitas Terbangun</label>
										<input type="text" name="kapasitas_terbangun" class="form-control" placeholder="Kapasitas Terbangun">
									</div>
									<div class="form-group">
										<label>Material Konstruksi</label>
										<input type="text" name="material_konstruksi" class="form-control" placeholder="Material Konstruksi">
									</div>
									<div class="form-group">
										<label>Sistem Pemeliharaan</label>
										<input type="text" name="sistem_pemeliharaan" class="form-control" placeholder="Sistem Pemeliharaan">
									</div>
									<div class="form-group">
										<label>Deskripsi</label>
										<textarea name="deskripsi" class="form-control" placeholder="Deskripsi"></textarea>
									</div>
									<div class="form-group">
										<label>Asbuilt Drawing Jaringan</label>
										<select name="asbuilt_drawing_jaringan" class="form-control">
											<option></option>
											<option value="ada">ada</option>
											<option value="tidak ada">tidak ada</option>
										</select>
									</div>
									<div class="form-group">
										<label>Peta Posisi Pelanggan</label>
										<select name="peta_posisi_pelanggan" class="form-control">
											<option></option>
											<option value="ada">ada</option>
											<option value="tidak ada">tidak ada</option>
										</select>
									</div>
									<div class="form-group" align="right">
										<br>
										<a href="#extra" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="extra">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Keberadaan Iuran</label>
										<select name="keberadaan_iuran" class="form-control">
											<option></option>
											<option value="ada">ada</option>
											<option value="tidak ada">tidak ada</option>
										</select>
									</div>
									<div class="form-group">
										<label>Besaran Tarif</label>
										<input type="text" name="besaran_tarif" class="form-control" placeholder="Besaran Tarif">
									</div>
									<div class="form-group">
										<label>Minat Untuk Peningkat</label>
										<select name="minat_utk_peningkat" class="form-control">
											<option></option>
											<option value="minat">minat</option>
											<option value="tidak minat">tidak minat</option>
										</select>
									</div>
									<div class="form-group">
										<label>Peningkatan Retribusi</label>
										<select name="peningkatan_retribusi" class="form-control">
											<option></option>
											<option value="berminat">berminat</option>
											<option value="tidak bersedia">tidak bersedia</option>
										</select>
									</div>
									<div class="form-group">
										<label>Besaran Retribusi</label>
										<input type="text" name="besaran_retribusi" class="form-control" placeholder="Besaran Retribusi">
									</div>
									<div class="form-group" align="right">
										<br>
										<a href="#gbr_umum" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
								<div class="col-md-6">

								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="gbr_umum">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Gambar Umum</label>
										<textarea name="gbr_umum" class="form-control" placeholder="Gambar Umum"></textarea>
									</div>
									<div class="form-group">
										<label>Hambatan Teknis</label>
										<textarea name="hambatan_teknis" class="form-control" placeholder="Hambatan Teknis"></textarea>
									</div>
									<div class="form-group">
										<label>Hambatan Non Teknis</label>
										<textarea name="hambatan_non_teknis" class="form-control" placeholder="Hambatan Non Teknis"></textarea>
									</div>
									<div class="form-group" align="right">
										<br>
										<a href="#potensi" class="btn btn-primary"role="tab" data-toggle="tab">Next</a>
									</div>
								</div>
								<div class="col-md-6">

								</div>
							</div>
						  </div>
						  <div role="tabpanel" class="tab-pane" id="potensi">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Potensi Pengembangan</label><br>
										<input type="radio" name="tdk_ada" value="ya" class="f" placeholder="Besaran Retribusi">Tidak Ada
										<input type="radio" name="tdk_ada" value="-" class="" placeholder="Besaran Retribusi">Ada
									</div>
									<div class="form-group">
										<label>Letak</label>
										<input type="text" name="letak" class="form-control" placeholder="Letak">
									</div>
									<div class="form-group">
										<label>Luas</label>
										<input type="text" name="luas" class="form-control" placeholder="Luas">
									</div>
									<div class="form-group">
										<label>Pemilik</label>
										<input type="text" name="pemilik" class="form-control" placeholder="Pemilik">
									</div>

								</div>

								<div class="col-md-6">
									<div class="form-group">
										<label>Minat Sikap Masyarakat</label>
										<select name="minat_sikap_masyarakat" class="form-control">
											<option></option>
											<option value="antusias">Antusias</option>
											<option value="biasa saja">Biasa Saja</option>
										</select>
									</div>
									<div class="form-group">
										<label>Keberadaan Lembaga</label>
										<select name="keberadaan_lembaga" class="form-control">
											<option></option>
											<option value="ada">Ada</option>
											<option value="tidak ada">Tidak Ada</option>
										</select>
									</div>
									<div class="form-group">
										<label>Keaktifan Kelembagaan</label>
										<input type="text" name="keaktifan_lembaga" class="form-control" placeholder="Keaktifan Kelembaagaan">
									</div>
									<div align="right">
										<input type="submit" name="btnSubmit" class="btn btn-primary" value="Submit">
									</div>
								<div>
							</div>
						  </div>
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'template/footer.php' ?>
<script>
	$("#kecamatan").change(function(){
		var id_kecamatan = $("#kecamatan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('admin/kelurahan')?>",
			type: "POST",
			data	: "id_kecamatan="+id_kecamatan,
			success : function (msg) {
				document.getElementById("kelurahan").disabled = false;
				$("#kelurahan").html(msg);
				//$("#kelurahan").css("color","black");
			}
		});
	});

</script>
