<?php include 'template/header.php' ?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="header">
                        <h4 class="title">Data Lokasi Sumber Air</h4>
                    </div>
					<div class="content">
						<?php
						if($this->session->flashdata('insert'))
						{
							?>
							<div class="alert alert-success">
								<button type="button" aria-hidden="true" class="close">×</button>
								<span><b> Info - </b> <?php echo $this->session->flashdata('insert'); ?></span>
                            </div>
							<?php
						}
						?>
						<?php
						if($this->session->flashdata('update'))
						{
							?>
							<div class="alert alert-info">
								<button type="button" aria-hidden="true" class="close">×</button>
								<span><b> Info - </b> <?php echo $this->session->flashdata('update'); ?></span>
                            </div>
							<?php
						}
						?>

						<?php
						if($this->session->flashdata('delete'))
						{
							?>
							<div class="alert alert-info">
								<button type="button" aria-hidden="true" class="close">×</button>
								<span><b> Info - </b> <?php echo $this->session->flashdata('delete'); ?></span>
                            </div>
							<?php
						}
						?>
						<?php if ($this->session->flashdata('error_upload')): ?>
							<div class="alert alert-danger">
								<button type="button" aria-hidden="true" class="close">×</button>
								<span><b> Info - </b> <?php echo $this->session->flashdata('error_upload'); ?></span>
              </div>
						<?php endif; ?>
						<form action="<?php echo base_url('admin/convert_to_pdf'); ?>" method="post">
							<div class="col-md-4">
								<div class="form-group">
									<label>Kecamatan</label>
									<select class="form-control" name="kecamatan" id="kecamatan">
										<option value="-">Kecamatan</option>
										<?php
										foreach($kecamatan->result() as $kecamatan)
										{
											?>
											<option value="<?php echo $kecamatan->id_kecamatan; ?>"><?php echo $kecamatan->nm_kecamatan; ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>

							<div class="col-md-4">
								<div class="form-group">
									<label>Kelurahan</label>
									<select class="form-control" name="kelurahan" id="kelurahan">
										<option value="-">Kelurahan</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<br>
								<input type="submit" name="btnSubmit" class="btn btn-primary" value="PDF">
							</div>
						</form>

						<table class="table table-hover table-striped" id="myTables">
							<thead>
								<th>No.</th>
								<th>ID</th>
								<th>Kecamatan</th>
								<th>Kelurahan</th>
								<th>Alamat</th>
								<th>Luas Lahan</th>
								<th>Longitude, Latitude</th>
								<th>Status Lahan</th>
								<th>Tahun Pembangunan</th>
								<th>Foto</th>
								<th></th>
								<th></th>
							</thead>
							<tbody>
								<?php
								$no = 1;
								foreach($sumber_air->result() as $row){
									?>
									<tr>
										<td><?php echo $no; ?></td>
										<td><?php echo $row->id_sa; ?></td>
										<td><?php echo $row->nm_kecamatan; ?></td>
										<td><?php echo $row->nm_desa; ?></td>
										<td><?php echo $row->almt; ?></td>
										<td><?php echo $row->ls_lahan; ?></td>
										<td><?php echo $row->longitude.", ".$row->latitude; ?></td>
										<td><?php echo $row->status_lahan; ?></td>
										<td><?php echo $row->thn_pembangunan; ?></td>
										<td><img class="img img-thumbnail" src="<?php echo base_url('uploads'); ?>/<?php echo $row->foto; ?>"></td>
										<td><a href="<?php echo base_url('admin/detail_info'); ?>/<?php echo $row->id_sa; ?>">Detail</a></td>
										<td>
											<a href="<?php echo base_url('admin/edit_data_sumber_air'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
											<a href="<?php echo base_url('admin/hapus_data_sumber_air'); ?>/<?php echo $row->id_sa; ?>" class="btn btn-danger" onclick="return confirm('Apakah anda yakin ingin menghapus data?')"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<?php
									$no++;
								}
								?>
							</body>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php include 'template/footer.php' ?>
<script>
	$(document).ready(function() {
		$('#myTables').DataTable();
	} );

	$("#kecamatan").change(function(){
		var id_kecamatan = $("#kecamatan option:selected").val();
		$.ajax({
			url: "<?php echo site_url('admin/kelurahan')?>",
			type: "POST",
			data	: "id_kecamatan="+id_kecamatan,
			success : function (msg) {
				document.getElementById("kelurahan").disabled = false;
				$("#kelurahan").html(msg);
				//$("#kelurahan").css("color","black");
			}
		});
	});
</script>
