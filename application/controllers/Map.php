<?php
class Map extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('data_model');
	}

	function index()
	{
		$data['marker_b_selatan'] = $this->data_model->get_map_marker_b_selatan();
		$data['marker_b_timur'] = $this->data_model->get_map_marker_b_timur();
		$data['marker_b_utara'] = $this->data_model->get_map_marker_b_utara();
		$data['marker_b_tengah'] = $this->data_model->get_map_marker_b_tengah();
		$data['marker_b_barat'] = $this->data_model->get_map_marker_b_barat();
		$data['marker_tan_sareal'] = $this->data_model->get_map_marker_tan_sareal();
		$this->load->view('front/front_index',$data);
	}

}
