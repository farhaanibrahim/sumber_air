<?php
class Admin extends CI_Controller{

	function __construct()
	{
		parent::__construct();
		$this->load->model('data_model');
	}

	function index()
	{
		if($this->session->userdata('status')=='login'){
			$data['marker_b_selatan'] = $this->data_model->get_map_marker_b_selatan();
			$data['marker_b_timur'] = $this->data_model->get_map_marker_b_timur();
			$data['marker_b_utara'] = $this->data_model->get_map_marker_b_utara();
			$data['marker_b_tengah'] = $this->data_model->get_map_marker_b_tengah();
			$data['marker_b_barat'] = $this->data_model->get_map_marker_b_barat();
			$data['marker_tan_sareal'] = $this->data_model->get_map_marker_tan_sareal();
			$this->load->view('index',$data);
		} else {
			redirect(base_url('login'));
		}
	}

	function input_data()
	{
		if($this->session->userdata('status')=='login'){
			$data['kecamatan'] = $this->data_model->get_kecamatan();
			$this->load->view('input_data_sumber_air',$data);
		} else {
			redirect(base_url('login'));
		}
	}
	function kelurahan(){
		if($this->data_model->kelurahan($this->input->post("id_kecamatan"))->num_rows() > 1){
			echo "<option value='-'>Pilih Kelurahan Disini</option>";
			foreach($this->data_model->kelurahan($this->input->post("id_kecamatan"))->result() as $row){
				echo "<option value='$row->id_desa'>$row->nm_desa</option>";
			}
		}else{
			echo 'kosong';
		}
	}

	function proses_input_data()
	{
		if($this->session->userdata('status')=='login')
		{
			$config = array(
				'upload_path' => "./uploads/",
				'allowed_types' => "jpg|png|jpeg",
				'overwrite' => TRUE,
				'max_size' => "10240000", // Can be set to particular file size , here it is 10 MB(2048 Kb)
				'max_height' => "3264",
				'max_width' => "2448"
			);
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if($this->upload->do_upload('foto'))
			{
				$count_record = $this->data_model->count_record();
				$id_sa = "SA".$count_record;
				$lokasi = array(
					'id_sa'=>$id_sa,
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'rt'=>$this->input->post('rt'),
					'rw'=>$this->input->post('rw'),
					'almt'=>$this->input->post('almt'),
					'no_sk_lh'=>$this->input->post('no_sk_lh'),
					'no_sk_kel'=>$this->input->post('no_sk_kel'),
					'longitude'=>$this->input->post('longitude'),
					'latitude'=>$this->input->post('latitude'),
					'ls_lahan'=>$this->input->post('ls_lahan'),
					'ls_lahan_terbangun'=>$this->input->post('ls_lahan_terbangun'),
					'perkiraan_sisa_lhn'=>$this->input->post('perkiraan_sisa_lhn'),
					'status_lahan'=>$this->input->post('status_lahan'),
					'pendanaan'=>$this->input->post('pendanaan'),
					'thn_pembangunan'=>$this->input->post('thn_pembangunan'),
					'foto'=>$this->upload->data('file_name')
				);
				$sis_air_minum = array(
					'id_sa'=>$id_sa,
					'sumber_air'=>$this->input->post('sumber_air'),
					'arah_mata_air'=>$this->input->post('arah_mata_air'),
					'kedalaman'=>$this->input->post('kedalaman'),
					'debit_liter'=>$this->input->post('debit_liter'),
					'debit_detik'=>$this->input->post('debit_detik'),
					'debit_air'=>$this->input->post('debit_air'),
					'pjg_pipa'=>$this->input->post('pjg_pipa'),
					'kebersihan'=>$this->input->post('kebersihan'),
					'fungsi'=>$this->input->post('fungsi'),
					'kondisi'=>$this->input->post('kondisi'),
					'pemeliharaan'=>$this->input->post('pemeliharaan'),
					'penjelasan_sam'=>$this->input->post('penjelasan_sam')
				);
				$jar_pipa = array(
					'id_sa'=>$id_sa,
					'jml_sr_terpasang'=>$this->input->post('jml_sr_terpasang'),
					'kondisi_kelancaran_sis_pengalir'=>$this->input->post('kondisi_kelancaran_sis_pengalir'),
					'kondisi_perawatan_sis_pengalir'=>$this->input->post('kondisi_perawatan_sis_pengalir'),
					'kondisi_fungsi_sis_pengalir'=>$this->input->post('kondisi_fungsi_sis_pengalir'),
					'ketersediaan_hidran'=>$this->input->post('ketersediaan_hidran'),
					'jml_hidran'=>$this->input->post('jml_hidran'),
					'meteran'=>$this->input->post('meteran'),
					'jml_meteran'=>$this->input->post('jml_meteran'),
					'pemeliharaan_sis_jaringan'=>$this->input->post('pemeliharaan_sis_jaringan'),
					'penjelasan_jar_pipa'=>$this->input->post('penjelasan_jar_pipa'),
					'lebar_jln'=>$this->input->post('lebar_jln')
				);
				$sis_pengolahan = array(
					'id_sa'=>$id_sa,
					'sis_pengolahan'=>$this->input->post('sis_pengolahan'),
					'bangunan_pelengkap'=>$this->input->post('bangunan_pelengkap'),
					'panjang'=>$this->input->post('panjang'),
					'lebar'=>$this->input->post('lebar'),
					'tinggi'=>$this->input->post('tinggi'),
					'jari_jari'=>$this->input->post('jari-jari'),
					'kapasitas_terbangun'=>$this->input->post('kapasitas_terbangun'),
					'material_konstruksi'=>$this->input->post('material_konstruksi'),
					'sistem_pemeliharaan'=>$this->input->post('sistem_pemeliharaan'),
					'deskripsi'=>$this->input->post('deskripsi'),
					'asbuilt_drawing_jaringan'=>$this->input->post('asbuilt_drawing_jaringan'),
					'peta_posisi_pelanggan'=>$this->input->post('peta_posisi_pelanggan')
				);
				$ekonomi = array(
					'id_sa'=>$id_sa,
					'keberadaan_iuran'=>$this->input->post('keberadaan_iuran'),
					'besaran_tarif'=>$this->input->post('besaran_tarif'),
					'minat_utk_peningkat'=>$this->input->post('minat_utk_peningkat'),
					'peningkatan_retribusi'=>$this->input->post('peningkatan_retribusi'),
					'besaran_retribusi'=>$this->input->post('besaran_retribusi')
				);
				$gbr_daerah = array(
					'id_sa'=>$id_sa,
					'gbr_umum'=>$this->input->post('gbr_umum'),
					'hambatan_teknis'=>$this->input->post('hambatan_teknis'),
					'hambatan_non_teknis'=>$this->input->post('hambatan_non_teknis')
				);
				$potensi = array(
					'id_sa'=>$id_sa,
					'tdk_ada'=>$this->input->post('tdk_ada'),
					'letak'=>$this->input->post('letak'),
					'luas'=>$this->input->post('luas'),
					'pemilik'=>$this->input->post('pemilik'),
					'minat_sikap_masyarakat'=>$this->input->post('minat_sikap_masyarakat'),
					'keberadaan_lembaga'=>$this->input->post('keberadaan_lembaga'),
					'keaktifan_lembaga'=>$this->input->post('keaktifan_lembaga')
				);
				$this->data_model->input_lokasi($lokasi);
				$this->data_model->input_sis_air_minum($sis_air_minum);
				$this->data_model->input_jar_pipa($jar_pipa);
				$this->data_model->input_sis_pengolahan($sis_pengolahan);
				$this->data_model->input_ekonomi($ekonomi);
				$this->data_model->input_gbr_daerah($gbr_daerah);
				$this->data_model->input_potensi($potensi);

				$this->session->set_flashdata('insert','Data berhasil diinput');

				redirect(base_url('admin/sumber_air'));
			}
			else
			{
				$count_record = $this->data_model->count_record();
				$id_sa = "SA".$count_record;
				$lokasi = array(
					'id_sa'=>$id_sa,
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'rt'=>$this->input->post('rt'),
					'rw'=>$this->input->post('rw'),
					'almt'=>$this->input->post('almt'),
					'no_sk_lh'=>$this->input->post('no_sk_lh'),
					'no_sk_kel'=>$this->input->post('no_sk_kel'),
					'longitude'=>$this->input->post('longitude'),
					'latitude'=>$this->input->post('latitude'),
					'ls_lahan'=>$this->input->post('ls_lahan'),
					'ls_lahan_terbangun'=>$this->input->post('ls_lahan_terbangun'),
					'perkiraan_sisa_lhn'=>$this->input->post('perkiraan_sisa_lhn'),
					'status_lahan'=>$this->input->post('status_lahan'),
					'pendanaan'=>$this->input->post('pendanaan'),
					'thn_pembangunan'=>$this->input->post('thn_pembangunan'),
					'foto'=>''
				);
				$sis_air_minum = array(
					'id_sa'=>$id_sa,
					'sumber_air'=>$this->input->post('sumber_air'),
					'arah_mata_air'=>$this->input->post('arah_mata_air'),
					'kedalaman'=>$this->input->post('kedalaman'),
					'debit_liter'=>$this->input->post('debit_liter'),
					'debit_detik'=>$this->input->post('debit_detik'),
					'debit_air'=>$this->input->post('debit_air'),
					'pjg_pipa'=>$this->input->post('pjg_pipa'),
					'kebersihan'=>$this->input->post('kebersihan'),
					'fungsi'=>$this->input->post('fungsi'),
					'kondisi'=>$this->input->post('kondisi'),
					'pemeliharaan'=>$this->input->post('pemeliharaan'),
					'penjelasan_sam'=>$this->input->post('penjelasan_sam')
				);
				$jar_pipa = array(
					'id_sa'=>$id_sa,
					'jml_sr_terpasang'=>$this->input->post('jml_sr_terpasang'),
					'kondisi_kelancaran_sis_pengalir'=>$this->input->post('kondisi_kelancaran_sis_pengalir'),
					'kondisi_perawatan_sis_pengalir'=>$this->input->post('kondisi_perawatan_sis_pengalir'),
					'kondisi_fungsi_sis_pengalir'=>$this->input->post('kondisi_fungsi_sis_pengalir'),
					'ketersediaan_hidran'=>$this->input->post('ketersediaan_hidran'),
					'jml_hidran'=>$this->input->post('jml_hidran'),
					'meteran'=>$this->input->post('meteran'),
					'jml_meteran'=>$this->input->post('jml_meteran'),
					'pemeliharaan_sis_jaringan'=>$this->input->post('pemeliharaan_sis_jaringan'),
					'penjelasan_jar_pipa'=>$this->input->post('penjelasan_jar_pipa'),
					'lebar_jln'=>$this->input->post('lebar_jln')
				);
				$sis_pengolahan = array(
					'id_sa'=>$id_sa,
					'sis_pengolahan'=>$this->input->post('sis_pengolahan'),
					'bangunan_pelengkap'=>$this->input->post('bangunan_pelengkap'),
					'panjang'=>$this->input->post('panjang'),
					'lebar'=>$this->input->post('lebar'),
					'tinggi'=>$this->input->post('tinggi'),
					'jari_jari'=>$this->input->post('jari-jari'),
					'kapasitas_terbangun'=>$this->input->post('kapasitas_terbangun'),
					'material_konstruksi'=>$this->input->post('material_konstruksi'),
					'sistem_pemeliharaan'=>$this->input->post('sistem_pemeliharaan'),
					'deskripsi'=>$this->input->post('deskripsi'),
					'asbuilt_drawing_jaringan'=>$this->input->post('asbuilt_drawing_jaringan'),
					'peta_posisi_pelanggan'=>$this->input->post('peta_posisi_pelanggan')
				);
				$ekonomi = array(
					'id_sa'=>$id_sa,
					'keberadaan_iuran'=>$this->input->post('keberadaan_iuran'),
					'besaran_tarif'=>$this->input->post('besaran_tarif'),
					'minat_utk_peningkat'=>$this->input->post('minat_utk_peningkat'),
					'peningkatan_retribusi'=>$this->input->post('peningkatan_retribusi'),
					'besaran_retribusi'=>$this->input->post('besaran_retribusi')
				);
				$gbr_daerah = array(
					'id_sa'=>$id_sa,
					'gbr_umum'=>$this->input->post('gbr_umum'),
					'hambatan_teknis'=>$this->input->post('hambatan_teknis'),
					'hambatan_non_teknis'=>$this->input->post('hambatan_non_teknis')
				);
				$potensi = array(
					'id_sa'=>$id_sa,
					'tdk_ada'=>$this->input->post('tdk_ada'),
					'letak'=>$this->input->post('letak'),
					'luas'=>$this->input->post('luas'),
					'pemilik'=>$this->input->post('pemilik'),
					'minat_sikap_masyarakat'=>$this->input->post('minat_sikap_masyarakat'),
					'keberadaan_lembaga'=>$this->input->post('keberadaan_lembaga'),
					'keaktifan_lembaga'=>$this->input->post('keaktifan_lembaga')
				);
				$this->data_model->input_lokasi($lokasi);
				$this->data_model->input_sis_air_minum($sis_air_minum);
				$this->data_model->input_jar_pipa($jar_pipa);
				$this->data_model->input_sis_pengolahan($sis_pengolahan);
				$this->data_model->input_ekonomi($ekonomi);
				$this->data_model->input_gbr_daerah($gbr_daerah);
				$this->data_model->input_potensi($potensi);

				$this->session->set_flashdata('insert','Data berhasil diinput');

				redirect(base_url('admin/sumber_air'));
			}
		} else {
			redirect(base_url('login'));
		}
	}

	function sumber_air()
	{
		if($this->session->userdata('status')=='login'){
			$data['kecamatan'] = $this->data_model->get_kecamatan();
			$data['sumber_air'] = $this->data_model->get_data_sumber_air();
			$this->load->view('data_sumber_air',$data);
		} else {
			redirect(base_url('login'));
		}
	}

	function detail_info($id_sa)
	{
		if($this->session->userdata('status')=='login')
		{
			$data['detail'] = $this->data_model->get_detail_information($id_sa);
			$this->load->view('data_detail',$data);
		} else {
			redirect(base_url('login'));
		}
	}

	function edit_data_sumber_air($id_sa)
	{
		if($this->session->userdata('status')=='login')
		{
			$data['kecamatan'] = $this->data_model->get_kecamatan();
			$data['data'] = $this->data_model->get_detail_information($id_sa);

			$this->load->view('edit_data_sumber_air',$data);
		} else {
			redirect(base_url('login'));
		}
	}

	function proses_edit_data_sbr_air()
	{
		if($this->session->userdata('status')=='login')
		{

			$config = array(
				'upload_path' => "./uploads/",
				'allowed_types' => "jpg|png|jpeg",
				'overwrite' => TRUE,
				'max_size' => "10240000", // Can be set to particular file size , here it is 10 MB(2048 Kb)
				'max_height' => "3264",
				'max_width' => "2448"
			);
			$this->load->library('upload', $config);

			$this->upload->initialize($config);

			if($this->upload->do_upload('foto'))
			{
				unlink("./uploads/".$this->input->post('foto_lama'));
				$id_sa = $this->input->post('id_sa');
				$lokasi = array(
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'rt'=>$this->input->post('rt'),
					'rw'=>$this->input->post('rw'),
					'almt'=>$this->input->post('almt'),
					'no_sk_lh'=>$this->input->post('no_sk_lh'),
					'no_sk_kel'=>$this->input->post('no_sk_kel'),
					'longitude'=>$this->input->post('longitude'),
					'latitude'=>$this->input->post('latitude'),
					'ls_lahan'=>$this->input->post('ls_lahan'),
					'ls_lahan_terbangun'=>$this->input->post('ls_lahan_terbangun'),
					'perkiraan_sisa_lhn'=>$this->input->post('perkiraan_sisa_lhn'),
					'status_lahan'=>$this->input->post('status_lahan'),
					'pendanaan'=>$this->input->post('pendanaan'),
					'thn_pembangunan'=>$this->input->post('thn_pembangunan'),
					'foto'=>$this->upload->data('file_name')
				);
				$sis_air_minum = array(
					'sumber_air'=>$this->input->post('sumber_air'),
					'arah_mata_air'=>$this->input->post('arah_mata_air'),
					'kedalaman'=>$this->input->post('kedalaman'),
					'debit_liter'=>$this->input->post('debit_liter'),
					'debit_detik'=>$this->input->post('debit_detik'),
					'debit_air'=>$this->input->post('debit_air'),
					'pjg_pipa'=>$this->input->post('pjg_pipa'),
					'kebersihan'=>$this->input->post('kebersihan'),
					'fungsi'=>$this->input->post('fungsi'),
					'kondisi'=>$this->input->post('kondisi'),
					'pemeliharaan'=>$this->input->post('pemeliharaan'),
					'penjelasan_sam'=>$this->input->post('penjelasan_sam')
				);
				$jar_pipa = array(
					'jml_sr_terpasang'=>$this->input->post('jml_sr_terpasang'),
					'kondisi_kelancaran_sis_pengalir'=>$this->input->post('kondisi_kelancaran_sis_pengalir'),
					'kondisi_perawatan_sis_pengalir'=>$this->input->post('kondisi_perawatan_sis_pengalir'),
					'kondisi_fungsi_sis_pengalir'=>$this->input->post('kondisi_fungsi_sis_pengalir'),
					'ketersediaan_hidran'=>$this->input->post('ketersediaan_hidran'),
					'jml_hidran'=>$this->input->post('jml_hidran'),
					'meteran'=>$this->input->post('meteran'),
					'jml_meteran'=>$this->input->post('jml_meteran'),
					'pemeliharaan_sis_jaringan'=>$this->input->post('pemeliharaan_sis_jaringan'),
					'penjelasan_jar_pipa'=>$this->input->post('penjelasan_jar_pipa'),
					'lebar_jln'=>$this->input->post('lebar_jln')
				);
				$sis_pengolahan = array(
					'sis_pengolahan'=>$this->input->post('sis_pengolahan'),
					'bangunan_pelengkap'=>$this->input->post('bangunan_pelengkap'),
					'panjang'=>$this->input->post('panjang'),
					'lebar'=>$this->input->post('lebar'),
					'tinggi'=>$this->input->post('tinggi'),
					'jari_jari'=>$this->input->post('jari-jari'),
					'kapasitas_terbangun'=>$this->input->post('kapasitas_terbangun'),
					'material_konstruksi'=>$this->input->post('material_konstruksi'),
					'sistem_pemeliharaan'=>$this->input->post('sistem_pemeliharaan'),
					'deskripsi'=>$this->input->post('deskripsi'),
					'asbuilt_drawing_jaringan'=>$this->input->post('asbuilt_drawing_jaringan'),
					'peta_posisi_pelanggan'=>$this->input->post('peta_posisi_pelanggan')
				);
				$ekonomi = array(
					'keberadaan_iuran'=>$this->input->post('keberadaan_iuran'),
					'besaran_tarif'=>$this->input->post('besaran_tarif'),
					'minat_utk_peningkat'=>$this->input->post('minat_utk_peningkat'),
					'peningkatan_retribusi'=>$this->input->post('peningkatan_retribusi'),
					'besaran_retribusi'=>$this->input->post('besaran_retribusi')
				);
				$gbr_daerah = array(
					'id_sa'=>$id_sa,
					'gbr_umum'=>$this->input->post('gbr_umum'),
					'hambatan_teknis'=>$this->input->post('hambatan_teknis'),
					'hambatan_non_teknis'=>$this->input->post('hambatan_non_teknis')
				);
				$potensi = array(
					'tdk_ada'=>$this->input->post('tdk_ada'),
					'letak'=>$this->input->post('letak'),
					'luas'=>$this->input->post('luas'),
					'pemilik'=>$this->input->post('pemilik'),
					'minat_sikap_masyarakat'=>$this->input->post('minat_sikap_masyarakat'),
					'keberadaan_lembaga'=>$this->input->post('keberadaan_lembaga'),
					'keaktifan_lembaga'=>$this->input->post('keaktifan_lembaga')
				);
				$this->data_model->edit_lokasi($lokasi,$id_sa);
				$this->data_model->edit_sis_air_minum($sis_air_minum,$id_sa);
				$this->data_model->edit_jar_pipa($jar_pipa,$id_sa);
				$this->data_model->edit_sis_pengolahan($sis_pengolahan,$id_sa);
				$this->data_model->edit_ekonomi($ekonomi,$id_sa);
				$this->data_model->edit_gbr_daerah($gbr_daerah,$id_sa);
				$this->data_model->edit_potensi($potensi,$id_sa);

				$this->session->set_flashdata('update','Data berhasil Diupdate');

				redirect(base_url('admin/sumber_air'));
			} else {
				$id_sa = $this->input->post('id_sa');
				$lokasi = array(
					'kecamatan'=>$this->input->post('kecamatan'),
					'kelurahan'=>$this->input->post('kelurahan'),
					'rt'=>$this->input->post('rt'),
					'rw'=>$this->input->post('rw'),
					'almt'=>$this->input->post('almt'),
					'no_sk_lh'=>$this->input->post('no_sk_lh'),
					'no_sk_kel'=>$this->input->post('no_sk_kel'),
					'longitude'=>$this->input->post('longitude'),
					'latitude'=>$this->input->post('latitude'),
					'ls_lahan'=>$this->input->post('ls_lahan'),
					'ls_lahan_terbangun'=>$this->input->post('ls_lahan_terbangun'),
					'perkiraan_sisa_lhn'=>$this->input->post('perkiraan_sisa_lhn'),
					'status_lahan'=>$this->input->post('status_lahan'),
					'pendanaan'=>$this->input->post('pendanaan'),
					'thn_pembangunan'=>$this->input->post('thn_pembangunan'),
					'foto'=>$this->input->post('foto_lama')
				);
				$sis_air_minum = array(
					'sumber_air'=>$this->input->post('sumber_air'),
					'arah_mata_air'=>$this->input->post('arah_mata_air'),
					'kedalaman'=>$this->input->post('kedalaman'),
					'debit_liter'=>$this->input->post('debit_liter'),
					'debit_detik'=>$this->input->post('debit_detik'),
					'debit_air'=>$this->input->post('debit_air'),
					'pjg_pipa'=>$this->input->post('pjg_pipa'),
					'kebersihan'=>$this->input->post('kebersihan'),
					'fungsi'=>$this->input->post('fungsi'),
					'kondisi'=>$this->input->post('kondisi'),
					'pemeliharaan'=>$this->input->post('pemeliharaan'),
					'penjelasan_sam'=>$this->input->post('penjelasan_sam')
				);
				$jar_pipa = array(
					'jml_sr_terpasang'=>$this->input->post('jml_sr_terpasang'),
					'kondisi_kelancaran_sis_pengalir'=>$this->input->post('kondisi_kelancaran_sis_pengalir'),
					'kondisi_perawatan_sis_pengalir'=>$this->input->post('kondisi_perawatan_sis_pengalir'),
					'kondisi_fungsi_sis_pengalir'=>$this->input->post('kondisi_fungsi_sis_pengalir'),
					'ketersediaan_hidran'=>$this->input->post('ketersediaan_hidran'),
					'jml_hidran'=>$this->input->post('jml_hidran'),
					'meteran'=>$this->input->post('meteran'),
					'jml_meteran'=>$this->input->post('jml_meteran'),
					'pemeliharaan_sis_jaringan'=>$this->input->post('pemeliharaan_sis_jaringan'),
					'penjelasan_jar_pipa'=>$this->input->post('penjelasan_jar_pipa'),
					'lebar_jln'=>$this->input->post('lebar_jln')
				);
				$sis_pengolahan = array(
					'sis_pengolahan'=>$this->input->post('sis_pengolahan'),
					'bangunan_pelengkap'=>$this->input->post('bangunan_pelengkap'),
					'panjang'=>$this->input->post('panjang'),
					'lebar'=>$this->input->post('lebar'),
					'tinggi'=>$this->input->post('tinggi'),
					'jari_jari'=>$this->input->post('jari-jari'),
					'kapasitas_terbangun'=>$this->input->post('kapasitas_terbangun'),
					'material_konstruksi'=>$this->input->post('material_konstruksi'),
					'sistem_pemeliharaan'=>$this->input->post('sistem_pemeliharaan'),
					'deskripsi'=>$this->input->post('deskripsi'),
					'asbuilt_drawing_jaringan'=>$this->input->post('asbuilt_drawing_jaringan'),
					'peta_posisi_pelanggan'=>$this->input->post('peta_posisi_pelanggan')
				);
				$ekonomi = array(
					'keberadaan_iuran'=>$this->input->post('keberadaan_iuran'),
					'besaran_tarif'=>$this->input->post('besaran_tarif'),
					'minat_utk_peningkat'=>$this->input->post('minat_utk_peningkat'),
					'peningkatan_retribusi'=>$this->input->post('peningkatan_retribusi'),
					'besaran_retribusi'=>$this->input->post('besaran_retribusi')
				);
				$gbr_daerah = array(
					'id_sa'=>$id_sa,
					'gbr_umum'=>$this->input->post('gbr_umum'),
					'hambatan_teknis'=>$this->input->post('hambatan_teknis'),
					'hambatan_non_teknis'=>$this->input->post('hambatan_non_teknis')
				);
				$potensi = array(
					'tdk_ada'=>$this->input->post('tdk_ada'),
					'letak'=>$this->input->post('letak'),
					'luas'=>$this->input->post('luas'),
					'pemilik'=>$this->input->post('pemilik'),
					'minat_sikap_masyarakat'=>$this->input->post('minat_sikap_masyarakat'),
					'keberadaan_lembaga'=>$this->input->post('keberadaan_lembaga'),
					'keaktifan_lembaga'=>$this->input->post('keaktifan_lembaga')
				);
				$this->data_model->edit_lokasi($lokasi,$id_sa);
				$this->data_model->edit_sis_air_minum($sis_air_minum,$id_sa);
				$this->data_model->edit_jar_pipa($jar_pipa,$id_sa);
				$this->data_model->edit_sis_pengolahan($sis_pengolahan,$id_sa);
				$this->data_model->edit_ekonomi($ekonomi,$id_sa);
				$this->data_model->edit_gbr_daerah($gbr_daerah,$id_sa);
				$this->data_model->edit_potensi($potensi,$id_sa);

				$this->session->set_flashdata('error_upload',$this->upload->display_errors());
				$this->session->set_flashdata('update','Data berhasil Diupdate');

				redirect(base_url('admin/sumber_air'));
			}

		} else {
			redirect(base_url('login'));
		}
	}

	function hapus_data_sumber_air($id_sa)
	{
		if($this->session->userdata('status')=='login')
		{
			$this->data_model->hapus_data_t_lokasi($id_sa);
			$this->data_model->hapus_data_t_sis_air_minum($id_sa);
			$this->data_model->hapus_data_t_sistem_pengolahan($id_sa);
			$this->data_model->hapus_data_t_potensi_pengembangan($id_sa);
			$this->data_model->hapus_data_t_jaringan_pipa($id_sa);
			$this->data_model->hapus_data_t_ekonomi($id_sa);
			$this->data_model->hapus_data_t_gambaran_daerah_pelayanan($id_sa);

			$this->session->set_flashdata('delete','Data berhasil dihapus');

			redirect(base_url('admin/sumber_air'));
		} else {
			redirect(base_url('login'));
		}
	}

	function setting_password()
	{
		if($this->session->userdata('status')=='login')
		{
			$data['user'] = $this->data_model->get_user(1);
			$this->load->view('edit_password',$data);
		} else {
			redirect(base_url('login'));
		}
	}

	function convert_to_pdf()
	{
		$kecamatan = $this->input->post('kecamatan');
		$kelurahan = $this->input->post('kelurahan');

		if($kecamatan!='-' && $kelurahan=='-')
		{
			$where = array('kecamatan'=>$kecamatan);
			$data['query']=$this->data_model->get_filter_data($where);
		} else if($kecamatan!='-'&&$kelurahan!='-')
		{
			$where = array('kecamatan'=>$kecamatan,'kelurahan'=>$kelurahan);
			$data['query']=$this->data_model->get_filter_data($where);
		} else
		{
			$data['query']=$this->data_model->get_filter_data2();
		}



		$html = $this->load->view('export_pdf',$data,true);

		$pdfFilePath = "Data Sumber Air.pdf";

		$this->load->library('m_pdf');

		$pdf = new mPDF();


		$pdfer = new mPDF('utf-8','A4-L');

		$pdfer->WriteHTML($html);

		$pdfer->output($pdfFilePath, "I");

		//$this->m_pdf->pdf->WriteHTML($html); // write the HTML into the PDF

		//$this->m_pdf->pdf->Output($pdfFilePath, "I");
	}
}
