<?php

class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Data_model');
	}
	
	function index()
	{
		if($this->session->userdata('status')=='login'){
			redirect(base_url('admin'));
		} else {
			$this->load->view('form_login');
		}
	}
	
	function proses_login()
	{
		if($this->session->userdata('status')=='login')
		{
			redirect(base_url('admin'));
		} else {
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			
			$where = array(
				'username'=>$username,
				'password'=>$password
			);
			
			$login = $this->Data_model->get_login($where);
			if($login->num_rows() > 0)
			{
				foreach($login->result() as $row){
					$data_session = array(
						'id'=>$row->id_user,
						'username'=>$row->username,
						'status'=>'login'
					);
					$this->session->set_userdata($data_session);
					redirect(base_url('admin'));
				}
			} else {
				$this->session->set_flashdata('login_failed','Wrong username or password!');
				redirect(base_url('login'));
			}
		}
	}
	
	function logout()
	{
		if($this->session->userdata('status')=='login'){
			$this->session->sess_destroy();
			redirect(base_url('login'));
		} else {
			redirect(base_url('login'));
		}
	}
}